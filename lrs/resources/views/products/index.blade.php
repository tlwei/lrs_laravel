@extends('admin.app')

@section('content')
 
    <a href="/option/{{base64_encode($app_url)}}">
        Back
    </a>

    <table class="table">
        <thead class="thead-dark">
            <tr>
                @php ($users = ['#','Name','UID','Job Title','Interest','Company','Password','Position','Address','Email','Mobile','Avatar','Mac Address','actions'])
                @foreach ($users as $user)
                  <th scope="col">{{$user}}</th>
                @endforeach
            </tr> 
        </thead>
        <tbody>
            @foreach ($products as $product)
            <tr>
                <td>{{ ++$i }}</td>

                @php ($users = [$product->name,$product->uid,$product->delegate,$product->interest,$product->company,$product->password,$product->position,$product->address,$product->email,$product->mobile_no,$product->avatar,$product->mac_address])
                @foreach ($users as $value)
                <td>{{ $value }}</td>
                @endforeach
                <td>
                    
                    <form action="{{ route('products.destroy',$product->id) }}" method="POST">
                                        
                        <a style="width:80px" class="btn btn-primary" href="{{ route('products.edit',base64_encode($product->id)) }}">Edit</a>

                        <a style="width:80px" class="btn btn-secondary" href="{{ route('mail',['user' => base64_encode($product->id),'mail' => base64_encode('single'),'app_url' => base64_encode($app_url)]) }}">Mail</a>
                    
                        @csrf
                        @method('DELETE')
                    
                        <button style="width:80px" type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    
    {!! $products->links() !!}
    
    <a href="{{ route('products.create', ['app_url' => base64_encode($app_url)]) }}">
      <button type="button" class="btn btn-light btn-lg btn-block">Create User</button>
    </a>
    <p></p> 
    <a href="{{ route('mailEdit',['app_url' => base64_encode($app_url)]) }}">
        <button type="button" class="btn btn-secondary btn-lg btn-block">Edit Mail Contents</button>
    </a> 
    <p></p> 
    @if (isset($product->id))
        <a href="{{ route('mail',['user' => base64_encode($product->id),'mail' => base64_encode($app_url),'app_url' => base64_encode($app_url)]) }}">
            <button type="button" class="btn btn-primary btn-lg btn-block">Send Mail to All Users</button>
        </a> 
    @endif
    
      
@endsection