@extends('admin.app')

@section('content')
    <a href="/proIndex/{{base64_encode($product->app_url)}}">
        Back
    </a>
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <form action="/userRegister" method="POST" enctype="multipart/form-data" >
        @csrf
        @method('POST')
   
        @php ($users = [$product->name,$product->delegate,$product->interest,$product->company,$product->password,$product->position,$product->address,$product->email,$product->mobile_no,$product->avatar])
        @php ($title = ['#','name','delegate','interest','company','password','position','address','email','mobile_no','avatar','actions'])
        @php ($label = ['#','Name','Job Title','Interest','Company','Password','Position','Address','Email','Mobile_no','Avatar','Actions'])

            <div class="form-group">
                 <input type='hidden' name="status" value="edit">
                 <input type='hidden' name="uid" value="{{$product->uid}}">
                 <input type='hidden' name="app_url" value="{{$product->app_url}}">
                @foreach ($users as $key => $item)
                    
                      @if ($title[$key+1] == 'avatar')
                        <div class="form-group row">
                            <label for={{$title[$key+1]}} class="col-sm-1 col-form-label">{{$label[$key+1]}}</label>
                            <div class="col-sm-10">
                                 <input maxlength="250" type="file" name="{{$title[$key+1]}}" value="{{ $item }}" class="form-control" placeholder="{{$title[$key+1]}}">
                            </div>
                          </div>
                      @elseif ($title[$key+1] == 'position')
                        <div class="form-group row">
                            <label for={{$title[$key+1]}} class="col-sm-1 col-form-label">{{$label[$key+1]}}</label>
                            <div class="col-sm-10">
                                <select class="col-sm-10" name="{{$title[$key+1]}}">
                                    <option value="exhibitor">exhibitor</option>
                                    <option value="supervisor">supervisor</option>
                                </select>
                            </div>
                        </div> 
                      @else
                        <div class="form-group row">
                            <label for={{$title[$key+1]}} class="col-sm-1 col-form-label">{{$label[$key+1]}}</label>
                            <div class="col-sm-10">
                                 <input maxlength="250" type="text" name="{{$title[$key+1]}}" value="{{ $item }}" class="form-control" placeholder="{{$title[$key+1]}}">
                            </div>
                        </div> 
                      @endif
                    
                @endforeach
                
            </div>  
            <button type="submit" class="btn btn-primary btn-lg btn-block">Edit User</button>
            <p></p> 
   
    </form>
@endsection