@extends('admin.app')

@section('content')
<a href="/proIndex/{{base64_encode($app_url)}}">
    Back 
</a>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="/mailContentUpdate" method="POST" enctype="multipart/form-data" >
    @csrf
  
    
        @if ($mailContent == '[]')
            @php ($users = ['','','','','','','','','','','','','','','','','','','',''])
        @else
            @php ($users = [$mailContent[0]->header_pic1,$mailContent[0]->header_pic2,$mailContent[0]->date1,$mailContent[0]->date2,$mailContent[0]->location1,$mailContent[0]->location2,$mailContent[0]->content_pic1,$mailContent[0]->content_pic2,$mailContent[0]->content1,$mailContent[0]->content2,$mailContent[0]->company,$mailContent[0]->address,$mailContent[0]->email,$mailContent[0]->phone,$mailContent[0]->facebook,$mailContent[0]->instagram,$mailContent[0]->twitter,$mailContent[0]->LinkedIn,$mailContent[0]->moreContent,$mailContent[0]->CEO])
        @endif
    
    @php ($title = ['app_url', 'header_pic1','header_pic2','date1','date2','location1','location2','content_pic1','content_pic2','content1','content2','company','address','email','phone','facebook','instagram','twitter','LinkedIn','moreContent','CEO'])

        <div class="form-group">
            <input type='hidden' name="app_url" value="{{base64_encode($app_url)}}">

            @foreach ($users as $key => $item)
            
                  @if (strpos($title[$key+1],'pic'))
                    <div class="form-group row">
                        <label for={{$title[$key+1]}} class="col-sm-1 col-form-label">{{$title[$key+1]}}</label>
                        <div class="col-sm-10">
                            <input type="file" name="{{$title[$key+1]}}" value="{{ $item }}" class="form-control" placeholder="{{$title[$key+1]}}">
                        </div>
                      </div>
                  @else
                    <div class="form-group row">
                        <label for={{$title[$key+1]}} class="col-sm-1 col-form-label">{{$title[$key+1]}}</label>
                        <div class="col-sm-10">
                            <input type="text" name="{{$title[$key+1]}}" value="{{ $item }}" class="form-control" placeholder="{{$title[$key+1]}}">
                        </div>
                    </div> 
                  @endif
                
            @endforeach
            
        </div>  
        <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
        <p></p> 
</form>
@endsection