@extends('admin.app')

@section('content')
<a href="/proIndex/{{base64_encode($app_url)}}">
    Back 
</a>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="/userRegister" method="POST" enctype="multipart/form-data" >
    @csrf
  
    @php ($users = ['','','','','','','','','',''])
    @php ($title = ['#','name','delegate','interest','company','password','position','address','email','mobile_no','avatar'])
    @php ($label = ['#','Name','Job Title','Interest','Company','Password','Position','Address','Email','Mobile_no','Avatar','Actions'])

        <div class="form-group">
            <input type='hidden' name="uid" value="new">
            <input type='hidden' name="app_url" value="{{base64_encode($app_url)}}">
            @foreach ($users as $key => $item)
                 
                  @if ($title[$key+1] == 'avatar')
                   
                  @elseif ($title[$key+1] == 'position')
                    <div class="form-group row">
                        <label for={{$title[$key+1]}} class="col-sm-1 col-form-label">{{$label[$key+1]}}</label>
                        <div class="col-sm-10">
                            <select class="col-sm-10" name="{{$title[$key+1]}}">
                                <option value="exhibitor">exhibitor</option>
                                <option value="supervisor">supervisor</option>
                            </select>
                        </div>
                    </div> 
                  @else
                    <div class="form-group row">
                        <label for={{$title[$key+1]}} class="col-sm-1 col-form-label">{{$label[$key+1]}}</label>
                        <div class="col-sm-10">
                             <input maxlength="250" type="text" name="{{$title[$key+1]}}" value="{{ $item }}" class="form-control" placeholder="{{$title[$key+1]}}">
                        </div>
                    </div> 
                  @endif
                
            @endforeach
            
        </div>  
        <button type="submit" class="btn btn-primary btn-lg btn-block">Create User</button>
        <p></p> 
</form>
@endsection