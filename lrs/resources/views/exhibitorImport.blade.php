@extends('admin.app')

@section('content')


<div class="container">
    
    <a href="/option/{{base64_encode($app_url)}}">
        Back
    </a>
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card"> 
                <div class="card-header">
                    Import Visitors Data <text style="color:red">::Only file .xlsx accepted.</text>
                </div>
                <div class="card-body">

                    <form action="{{ route('importE',['app_url' => $app_url ])}}" method="POST" enctype="multipart/form-data">
                        @csrf
                            <input type="file" name="file" class="form-control">
                        <br> 
                        <button class="btn btn-primary">Import Visitors Data</button>
                        <a class="btn btn-secondary" href="{{ route('exportVisitorData',['app_url' => $app_url ])}}">Export Visitors Data</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
   
@endsection