@extends('admin.app')

@section('content')
 
    <a href="/option/{{base64_encode($app_url)}}">
        Back
    </a>

    <table class="table">
        <thead class="thead-dark">
            <tr>
                @php ($users = ['#','UID','Company','Title','First Name','Last Name','Job Title','Mobile','Email'])
                @foreach ($users as $user)
                  <th scope="col">{{$user}}</th>
                @endforeach
            </tr> 
        </thead>
        <tbody>
            @foreach ($products as $product)
            <tr>
                <td>{{ ++$i }}</td>

                @php ($users = [$product->uid,$product->company,$product->title,$product->first_name,$product->last_name,$product->delegate,$product->mobile,$product->email])
                @foreach ($users as $value)
                <td>{{ $value }}</td>
                @endforeach
                
            </tr>
            @endforeach
        </tbody>
    </table>
    
    {!! $products->links() !!}
    
      
@endsection