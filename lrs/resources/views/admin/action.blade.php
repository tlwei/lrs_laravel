@extends('admin.app')

@section('content')
    <a href="/option/{{$products[0]->app_url}}">
        Back
    </a>
    <table class="table">
        <thead class="thead-dark">
            <tr>
                @php ($users = ['#','Name','UID','Title','Interest','Company','Password','Position','Address','Email','User Name','Mobile','avatar','actions'])
                @foreach ($users as $user)
                  <th scope="col">{{$user}}</th>
                @endforeach
            </tr> 
        </thead>
        <tbody>
            @foreach ($products as $product)
            <tr>
                <td>{{ ++$i }}</td>

                @php ($users = [$product->name,$product->uid,$product->delegate,$product->interest,$product->company,$product->password,$product->position,$product->address,$product->email,$product->user_name,$product->mobile_no,$product->avatar])
                @foreach ($users as $value)
                <td>{{ $value }}</td>
                @endforeach
                <td>
                    <form action="{{ route('products.destroy',$product->id) }}" method="POST">
                                        
                        <a style="width:80px" class="btn btn-primary" href="{{ route('products.edit',$product->id) }}">Edit</a>
                    
                        @csrf
                        @method('DELETE')
                    
                        <button style="width:80px" type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    
    {!! $products->links() !!}
    <a href="{{ route('products.create') }}">
      <button type="button" class="btn btn-secondary btn-lg btn-block">Users CRUD</button>
    </a>
    <p></p> 
      
@endsection