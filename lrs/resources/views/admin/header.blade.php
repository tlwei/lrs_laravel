

<nav style="background-color:white" class="navbar navbar-expand-md shadow navbar-light navbar-laravel fixed-top">
    <div class="container">
        <a style="color:#7f7f7f;font-size:18px" class="">
            Lead Retrieval CMS Portal Admin
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
 
            </ul>
                <!-- Authentication Links -->
                <a class="nav-link" href="/user_admin/lrs">{{ __('Login') }}</a>
                <a class="nav-link" href="/user_admin/lrs">{{ __('Logout') }}</a>
                {{-- <a class="nav-link" href="register">{{ __('Register') }}</a> --}}
            </ul>
        </div>
    </div>
</nav>