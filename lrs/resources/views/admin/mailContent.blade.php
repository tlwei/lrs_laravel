<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
 
        <title>Lead Retrieval CMS Mail</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

        <!-- Styles -->
        <style>
            
            .container {
                background: white;
                color: #636b6f;
                width:600;
           
            }
            .header{
                padding:10;
            }
            
            .headerIcon { 
                margin-left:1%;
                width: 7%; 
                float: left; 
                position: relative;
            }

            .headerText { 
                margin-top:10px;
                margin-left:18px;
                width: 35%; 
                float: left; 
                position: relative;
            }

            @media only screen and (max-width: 1000px) {
                .headerText { 
                    margin-top:0px;
                    width: 35%; 
                    float: left; 
                    position: relative;
                }
              }

            .text {
                
                color:#7f7f7f;
                font-size:80%;
                font-weight:bold;
            }
  
            .image{
                width: 100%;
            }
            .contentImage{
                width: 100%;
             
            }
            .contentText{
                font-family: 'Nunito', sans-serif;
                background: #f7f7f7;
                padding:20px;
                color:rgb(56, 55, 55);
                font-weight:bold;
            }
            .footer{
                padding:20px;
            }

            .footerLeft { 
                float: left; 
                position: relative;
            }

            .footerAdjust { 
                width: 50%; 
            }

            @media only screen and (max-width: 1000px) {
                .footerAdjust { 
                    width: 80%; 
                }
              }
  

            .footerRight { 
                
                float: left; 
                position: relative;
            }

            .footerRightAdjust { 
                width: 25%; 
            }

            @media only screen and (max-width: 1000px) {
                .footerRightAdjust { 
                    top:4vh;
                    width: 50%; 
                }
              }

            .footerBar{
                color:black;
            }

            .footerBarLeft { 
                width: 60%; 
                float: left; 
                position: relative;
            }

            .footerBarRight { 
                width: 13%; 
                float: right; 
                position: relative;
            }

            @media only screen and (max-width: 1000px) {
                .footerBarRight { 
                    width: 33%; 
                    float: right; 
                    position: relative;
                }
            }

        </style>
    </head>
    <body>
        <div class="container">
 
            <div class="header">
                <div class="headerIcon">
                    {{--  "http://18.224.94.233/"  --}}
                    @php
                        $header1 = $content->header_pic1;
                        $header2 = $content->header_pic2;
                        $content1 = $content->content_pic1;
                        $content2 = $content->content_pic2;
                        $url = "http://sei-app.com/";
                        $header_pic1 = "{$url}app{$header1}";
                        $header_pic2 = "{$url}app{$header2}";
                        $content_pic1 = "{$url}app{$content1}";
                        $content_pic2 = "{$url}app{$content2}";
                        
                    @endphp
                    <img src="{{ asset($header_pic1)}}" alt="img" title="img" width=45 height=45 data-auto-embed="attachment"/>
                </div>
               
                <div class="headerText text">
                   {{--  <p></p>  --}}
                    <text>{!! $content->date1 !!}</text><br>
                    <text>{!! $content->location1 !!}</text>
                </div>
     
                <div class="headerIcon">
                    <img src="{{ asset($header_pic2)}}" alt="img" title="img" width=45 height=45 data-auto-embed="attachment"/>
                </div>
          
                <div class="headerText text">
                    {{--  <p></p>  --}}
                    <text>{!! $content->date2 !!}</text><br>
                    <text>{!! $content->location2 !!}</text>
                </div>
            </div>
            
            <div class="imageContainer">
                <img class="image" src="{{ asset($content_pic1)}}" alt="img" width="100%" title="img" data-auto-embed="attachment"/>
            </div>
            <div class="contentText">
                <text>Dear {!! $user_name !!},</text>
                <br><br>
                <text>
                    {!! $content->content1 !!}
                </text>
            </div>

            <div class="imageContainer">
                <img class="contentImage" src="{{ asset($content_pic2)}}" alt="img" width="100%" title="img" data-auto-embed="attachment"/>
            </div>

            <div class="contentText">
                <text>
                    {!! $content->content2 !!}
                </text>
            </div>
           
            <div class="footer">
                <div class="footerLeft footerAdjust">
                    <div>More About Exhibition</div>
                    <br>
                    <text>
                        {!! $content->moreContent !!}
                    </text>
                    <br><br>
                    <text>{!! $content->CEO !!}, CEO</text>
                </div>
                <div class="footerRight footerRightAdjust">
                    <div>Keep Connected</div>
                    <br>
                    <div>
                        <text>{!! $content->company !!}</text>
                    </div>
             
                    <div>
                        <text>{!! $content->address !!}</text>
                    </div>
               
                    <div>
                        <text>{!! $content->email !!}</text>
                    </div>

                    <div>
                        <text>{!! $content->phone !!}</text>
                    </div>
                    
                </div>
                <div class="footerRight footerRightAdjust"> 
                    <div>Contact Info</div>
                    <br>
                    <div>
                        <a href={!! $content->facebook !!}><text>Like our Facebook</text></a>
                    </div>

                    <div>
                        <a href={!! $content->instagram !!}><text>Follow our Twitter</text></a>
                    </div>
   
                    <div>
                        <a href={!! $content->twitter !!}><text>Add our LinkedIn</text></a>
                    </div>

                    <div>
                        <a href={!! $content->LinkedIn !!}><text>Like our Facebook</text></a>
                    </div>
                    <br><br><br>
                </div>
                
            </div>
           
            <div class="footerBar">
                <div class="footerBarLeft">
                    &copy;2019 Lead Retrieval CMS System. All Right Reserved | Design by LevineC
                </div>
                
                <div class="footerBarRight">Term</div>
                <div class="footerBarRight">Privacy Policy</div>
                <div class="footerBarRight">Company Info</div>
            </div>
            
            
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


    @include('sweet::alert')
</html>
