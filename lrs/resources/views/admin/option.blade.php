@extends('admin.app')

@section('content')
<div class="options">    
    <h4>{{$app_url}} . Welcome, Admin</h4>
    <div class="card">
      <div class="card-header">
       User Options
      </div> 
      <div class="card-body"> 
          {{--  base64_encode(  --}}
        <a href="{{ route('importExportView',['app_url' => base64_encode($app_url)]) }}">
            <button type="button" class="btn btn-primary btn-lg btn-block">Import or Export Users</button>
        </a> 
        <p></p> 
        {{-- <a href="{{ route('products.index',['app_url' => base64_encode($app_url)]) }}"> --}}
            
          <a href="/proIndex/{{base64_encode($app_url)}}">
          <button type="button" class="btn btn-light btn-lg btn-block">Users CRUD</button>
        </a>
        <p></p> 
        <a href="{{ route('importExhibitor',['app_url' => base64_encode($app_url)]) }}">
          <button type="button" class="btn btn-secondary btn-lg btn-block">Import or Export Visitors</button>
        </a> 
        <p></p> 
        <a href="/exhibitorsView/{{base64_encode($app_url)}}">
          <button type="button" class="btn btn-light btn-lg btn-block">View Visitors</button>
        </a> 
        <p></p> 
       
        <from>
          <a href="{{ route('action.index',['app_url' => base64_encode($app_url)]) }}">
            <button type="button" class="btn btn-secondary btn-lg btn-block">Qualifier CRUD</button>
          </a>
        </from>
      </div>
    </div>
</div>
    
@endsection