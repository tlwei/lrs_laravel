@extends('admin.app')

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="/api/app" method="POST" enctype="multipart/form-data" >
    {{ csrf_field() }}
  
    <a href="{{ route('viewAll') }}">
      <button type="button" class="btn btn-primary btn-lg btn-block">Exhibition Event CRUD </button>
    </a>
    <p></p> 
    @php
        $mytime = Carbon\Carbon::now();
        $today = $mytime->toDateTimeString();
    @endphp
    @php ($users = ['','rgb(241, 243, 244)',20,'arrow-forward','','black','white','black','',$today,$today,''])
    @php ($title = ['app_url','background_color','font_size','list_icon','main_title','title_color','btn_color','btn_text','user_id','start_date','end_date','footer_img'])
    @php ($label = ['Exhibition Short Form e.g-lrs/new','Exhibition App Background Color e.g-white/rgb(241, 243, 244)','Exhibition App Font Size e.g-20','Exhibition App Icon e.g-arrow-forward','Exhibition Name e.g-SEI Event Retrieval App','Exhibition App Title Color e.g-black','App Button Color e.g-white','App Button Text Color e.g-black','Exhibitor ID Short Form e.g-EX/NE (suggestion::First 2 character with your Exhibition Short Form)','Event Start Date','Event End Date','Footer logo'])

        <div class="form-group">
            
            @foreach ($users as $key => $item)
                 @if ($title[$key] == 'start_date' || $title[$key] == 'end_date')
                     <div class="form-group row">
                        <label for={{$title[$key]}} class="col-sm-2 col-form-label">{{$label[$key]}}</label>
                        <div class="col-sm-8">
                             <input required data-bv-notempty="true" maxlength="250" type="date" name="{{$title[$key]}}" value="{{ $item }}" class="form-control" placeholder="{{$label[$key]}}">
                        </div>
                    </div> 
                 @elseif($title[$key] == 'footer_img') 
                     <div class="form-group row">
                        <label for={{$title[$key]}} class="col-sm-2 col-form-label">{{$label[$key]}}</label>
                        <div class="col-sm-8">
                            <input required class="form-control" type="file" id="footer_img" name = "footer_img">
                        </div>
                    </div> 
                 @else
                     <div class="form-group row">
                        <label for={{$title[$key]}} class="col-sm-2 col-form-label">{{$label[$key]}}</label>
                        <div class="col-sm-8">
                             <input required data-bv-notempty="true" maxlength="250" type="text" name="{{$title[$key]}}" value="{{ $item }}" class="form-control" placeholder="{{$label[$key]}}">
                        </div>
                    </div> 
                 @endif
                
                
            @endforeach
            
        </div>  
        <button type="submit" class="btn btn-primary btn-lg btn-block">Create Event</button>
        <p></p> 
</form>
@endsection