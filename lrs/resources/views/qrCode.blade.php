@extends('app')

@section('content')
{{--  <a href="javascript:history.back()">Back</a>  --}}
<div class="options"> 
        
    <a href="{{route('navigation',['actions' => 'options','uid' => base64_encode($uid),'app_url' => base64_encode($app_url),'company' => base64_encode($password) ])}}">Back</a>
    <div class="visible-print text-center">
        {!! QrCode::size(350)->generate($qrcodeValue); !!}
    </div>
    <div class="visible-print text-center">
        Scan to login.
    </div>
</div>
@endsection