@extends('admin.app')

@section('content')
<a href="/create">
    Back
</a>
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <form action="/editEvent/{{base64_encode($product->id)}}" method="POST" enctype="multipart/form-data" >
        @csrf
        @method('POST')
   
        @php ($users = [$product->app_url,$product->background_color,$product->font_size,$product->list_icon,$product->main_title,$product->title_color,$product->btn_color,$product->btn_text,$product->footer_img,$product->user_id,$product->start_date,$product->end_date,$product->expired_status])
        @php ($title = ['#','app_url', 'background_color', 'font_size','list_icon','main_title','title_color','btn_color','btn_text','footer_img','user_id','start_date','end_date','expired_status'])
        @php ($label = ['#','app_url', 'background_color', 'font_size','list_icon','main_title','title_color','btn_color','btn_text','footer_img','user_id','start_date','end_date','expired_status'])

            <div class="form-group">
                 <input type='hidden' name="status" value="edit">
                 <input type='hidden' name="uid" value="{{$product->uid}}">
                 <input type='hidden' name="app_url" value="{{$product->app_url}}">
                @foreach ($users as $key => $item)
                    
                      @if ($title[$key+1] == 'footer_img')
                        <div class="form-group row">
                            <label for={{$title[$key+1]}} class="col-sm-2 col-form-label">{{$label[$key+1]}}</label>
                            <div class="col-sm-10">
                                 <input maxlength="250" type="file" name="{{$title[$key+1]}}" value="{{ $item }}" class="form-control" placeholder="{{$title[$key+1]}}">
                            </div>
                          </div>
                      @elseif ($title[$key+1] == 'expired_status')
                        <div class="form-group row">
                            <label for={{$title[$key+1]}} class="col-sm-2 col-form-label">{{$label[$key+1]}}</label>
                            <div class="col-sm-10">
                                <select class="col-sm-10" name="{{$title[$key+1]}}">
                                    <option value="0">Start Event</option>
                                    <option value="1">Close Event</option>
                                </select>
                            </div>
                        </div> 
                      @elseif ($title[$key+1] == 'start_date' || $title[$key+1] == 'end_date')
                        <div class="form-group row">
                            <label for={{$title[$key+1]}} class="col-sm-2 col-form-label">{{$label[$key+1]}}</label>
                            <div class="col-sm-10">
                                <input maxlength="250" type="date" name="{{$title[$key+1]}}" value="{{ $item }}" class="form-control" placeholder="{{$title[$key+1]}}">
                            </div>
                        </div> 
                      @else
                        <div class="form-group row">
                            <label for={{$title[$key+1]}} class="col-sm-2 col-form-label">{{$label[$key+1]}}</label>
                            <div class="col-sm-10">
                                 <input maxlength="250" type="text" name="{{$title[$key+1]}}" value="{{ $item }}" class="form-control" placeholder="{{$title[$key+1]}}">
                            </div>
                        </div> 
                      @endif
                    
                @endforeach
                
            </div>  
            <button type="submit" class="btn btn-primary btn-lg btn-block">Edit Event</button>
            <p></p> 
   
    </form>
@endsection