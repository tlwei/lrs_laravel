@extends('admin.app')

@section('content')
 
<a href="/create">
    Back
</a>

    <table class="table">
        <thead class="thead-dark">
            <tr>
                @php ($users = ['id','app_url','username','password', 'background_color', 'font_size','list_icon','main_title','title_color','btn_color','btn_text','footer_img','user_id','expired_status','Actions'])
                @foreach ($users as $user)
                  <th scope="col">{{$user}}</th>
                @endforeach
            </tr> 
        </thead>
        <tbody>
            @foreach ($products as $product)
            <tr>
                <td>{{ ++$i }}</td>

                @php ($users = [$product->app_url,$product->username,$product->password,$product->background_color,$product->font_size,$product->list_icon,$product->main_title,$product->title_color,$product->btn_color,$product->btn_text,$product->footer_img,$product->user_id,$product->expired_status])
                @foreach ($users as $value)
                <td>{{ $value }}</td>
                @endforeach
                <td>
                    <form action="/editEventView/{{base64_encode($product->id)}}" method="POST">  
                        @csrf
                        <button style="width:80px" type="submit" class="btn btn-primary">Edit</button>
                    </form>

                    <form action="/expired/{{base64_encode($product->id)}}" method="POST">

                        @csrf

                        <button style="width:80px" type="submit" class="btn btn-danger">Expried</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    
    {!! $products->links() !!}
    
   <a href="/create">
      <button type="button" class="btn btn-light btn-lg btn-block">Create Event</button>
    </a>
    <p></p> 

      
@endsection