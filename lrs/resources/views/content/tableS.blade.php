@extends('app')

@section('content') 
<div class="options">  
<a href="{{route('navigation',['actions' => 'leads','uid' => base64_encode($uid),'app_url' => base64_encode($app_url),'company' => base64_encode($password) ])}}">
  Back
</a>

<table class="table">
  <thead class="thead-dark">
    <tr>
        @php ($users = ['#','Name','UID','Title','Company','Email','Mobile'])
        @foreach ($users as $user)
          <th scope="col">{{$user}}</th>
        @endforeach
    </tr>
  </thead>
   
  <tbody>

      @foreach ($scannedUser as $key => $data)
      <tr>
        <th scope="row">{{$key+1}}</th>
        @php ($users = [$data->name,$data->scan_uid,$data->delegate,$data->company,$data->email,$data->mobile])
        @foreach ($users as $data)
          <th>{{$data}}</th>
        @endforeach
      </tr>
    @endforeach

  </tbody>
</table>
<div class="card-body">
  <a href="{{route('navigation',['actions' => 'exportActions','uid' => base64_encode($uid),'app_url' => base64_encode($app_url),'company' => base64_encode($password) ])}}">
  {{--  <a href="{{route('navigation',['actions' => 'exportScanned','uid' => base64_encode($uid),'app_url' => base64_encode($app_url),'company' => base64_encode($password) ])}}">  --}}
      <button type="button" class="btn btn-primary btn-lg btn-block">Export Leads and Collected Data</button>
  </a>
  <p></p>
  {{--  <a href="/">
    <button type="button" class="btn btn-secondary btn-lg btn-block">Logout User</button>
  </a>  --}}
</div>
</div>
@endsection