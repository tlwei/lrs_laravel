@extends('app')

@section('content')
<div class="options"> 
    <a href="{{route('navigation',['actions' => 'user_edit','uid' => base64_encode($uid),'app_url' => base64_encode($password),'company' => base64_encode($app_url) ])}}">Back</a>
    
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <form action="/changePassword" method="POST" enctype="multipart/form-data" >
        @csrf
        @method('POST')
        <input type='hidden' name="uid" value="{{$uid}}">
        <input type='hidden' name="app_url" value="{{$password}}">
        <input type='hidden' name="password" value="{{$app_url}}">
        <div class="form-group">
           
            <div class="form-group row">
                <label for='old_password' class="col-sm-2 col-form-label">Old Password</label>
                <div class="col-sm-10">
                    <input type="password" name="old_password" value="" class="form-control" placeholder="Old Password">
                </div>
            </div> 

            <div class="form-group row">
                <label for='new_password' class="col-sm-2 col-form-label">New Password</label>
                <div class="col-sm-10">
                    <input type="password" name="new_password" value="" class="form-control" placeholder="New Password">
                </div>
            </div>

            <div class="form-group row">
                <label for='confirm_password' class="col-sm-2 col-form-label">Confirm Password</label>
                <div class="col-sm-10">
                    <input type="password" name="confirm_password" value="" class="form-control" placeholder="Confirm Password">
                </div>
            </div>
        </div>  

        <button type="submit" class="btn btn-primary btn-lg btn-block">Change Password</button>

   
    </form>
</div>
@endsection