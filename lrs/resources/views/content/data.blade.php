@extends('app')

@section('content')
<div class="options">   
    <a href="{{route('navigation',['actions' => 'options','uid' => base64_encode($uid),'app_url' => base64_encode($app_url),'company' => base64_encode($password) ])}}">Back</a>
    
    <div class="card">
      <div class="card-header">
      </div>
      <div class="card-body"> 
        {{-- download --}}
            {{-- <a href="{{route('navigation',['actions' => 'exportNote','uid' => base64_encode($uid),'app_url' => base64_encode($app_url),'company' => base64_encode('-') ])}}">
                <button type="button" class="btn btn-secondary btn-lg btn-block">Export Note</button>
            </a>
            <p></p>  --}}
            <a href="{{route('navigation',['actions' => 'download','uid' => base64_encode($uid),'app_url' => base64_encode($app_url),'company' => base64_encode('-') ])}}">
              <button type="button" class="btn btn-primary btn-lg btn-block">Export Audio</button>
            </a>
            <p></p> 
            {{--  <a href="{{route('navigation',['actions' => 'exportActions','uid' => base64_encode($uid),'app_url' => base64_encode($app_url),'company' => base64_encode('-') ])}}">
              <button type="button" class="btn btn-primary btn-lg btn-block">Export Actions</button>
            </a>  --}}
      </div>
    </div>
</div>
    
@endsection