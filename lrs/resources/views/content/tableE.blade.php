@extends('app')

@section('content')
<div class="options"> 
 
<a href="{{route('navigation',['actions' => 'leads','uid' => base64_encode($uid),'app_url' => base64_encode($app_url),'company' => base64_encode($password) ])}}">

  Back
</a>

<table class="table">
  <thead class="thead-dark">
    <tr>
        @php ($users = ['#','Name','UID','Title','Email','Company','Mobile','Email','Address','Position'])
        @foreach ($users as $user)
          <th scope="col">{{$user}}</th>
        @endforeach
    </tr>
  </thead>
  <tbody>
    <tr> 
        <th scope="row">1</th>
        @php ($users = [$self[0]->name,$self[0]->uid,$self[0]->delegate,$self[0]->email,$self[0]->company,$self[0]->mobile_no,$self[0]->email,$self[0]->address,$self[0]->position])
        @foreach ($users as $data)
          <th>{{$data}}</th>
        @endforeach
    </tr>
    @if ($exhibitor)
      @foreach ($exhibitor as $key => $data)
        <tr>
          <td scope="row">{{$key+2}}</td>
          @php ($users = [$data->name,$data->uid,$data->delegate,$data->email,$data->company,$data->mobile_no,$data->email,$data->address,$data->position])
          @foreach ($users as $data)
            <th>{{$data}}</th>
          @endforeach
        </tr>
      @endforeach
    @endif
    
  </tbody>
</table> 

<div class="card-body">
  <a href="{{route('navigation',['actions' => 'exportSelf','uid' => base64_encode($uid),'app_url' => base64_encode($app_url),'company' => base64_encode('-') ])}}">
      <button type="button" class="btn btn-primary btn-lg btn-block">Export Self Data</button>
  </a>
  <p></p>

  @if ($exhibitor)
    <a href="{{route('navigation',['actions' => 'exportExhibitor','position' => base64_encode($self[0]->position),'app_url' => base64_encode($app_url),'company' => base64_encode($self[0]->company) ])}}">
        <button type="button" class="btn btn-secondary btn-lg btn-block">Export Exhibitors</button>
    </a>
  <p></p>
  @endif
  
  {{--  <a href="/">
    <button type="button" class="btn btn-secondary btn-lg btn-block">Logout User</button>
  </a>  --}}
</div>
</div>
@endsection