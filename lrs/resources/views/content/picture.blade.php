@extends('app')

@section('content')
<div class="options"> 
    <a href="{{route('navigation',['actions' => 'user_edit','uid' => base64_encode($uid),'app_url' => base64_encode($password),'company' => base64_encode($app_url) ])}}">Back</a>
    
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
     
    <form action="/changePicture" method="POST" enctype="multipart/form-data" >
        @csrf
        @method('POST')

        <input type="hidden" name="uid" value="{{$uid}}">
        <input type="hidden" name="app_url" value="{{$password}}">
        <div class="form-group">
            <div class="form-group row">
              <label for='profile' class="col-sm-2 col-form-label">Profile Picture</label>
              <div class="col-sm-8">
                  <input type="file" name="avatar" value="profile" class="form-control" placeholder="Profile Picture">
              </div>
            </div>
        </div>  
        <button type="submit" class="btn btn-primary btn-lg btn-block">Change Profile Pic</button>
        <p></p> 
   
    </form>
</div>
@endsection