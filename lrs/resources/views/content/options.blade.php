{{--  @php $url = $app_url; @endphp  --}}
@extends('app',compact("url"))
 
@section('content')
<div class="options">  
    <h4>Welcome, User</h4>
 
    <div class="card">
      <div class="card-header">
       User Options
      </div>  
      <div class="card-body">
         
        <a href="{{route('navigation',['actions' => 'leads','uid' => base64_encode($uid),'app_url' => base64_encode($app_url),'company' => base64_encode($password) ])}}">
            <button type="button" class="btn btn-link btn-lg btn-block">View Leads</button>
        </a>
        <p></p> 
        <from>
          <a href="{{route('navigation',['actions' => 'data','uid' => base64_encode($uid),'app_url' => base64_encode($app_url),'company' => base64_encode($password) ])}}">
            <button type="button" class="btn btn-primary btn-lg btn-block">Collected Data</button>
          </a>
        </from>
        <p></p> 
        <a href="{{route('navigation',['actions' => 'device','uid' => base64_encode($uid),'app_url' => base64_encode($app_url),'company' => base64_encode($password) ])}}">
          <button type="button" class="btn btn-light btn-lg btn-block">Login Devices</button>
        </a>
        <p></p> 
        <a href="{{route('navigation',['actions' => 'qrcode','uid' => base64_encode($uid),'app_url' => base64_encode($app_url),'company' => base64_encode($password) ])}}">
          <button type="button" class="btn btn-secondary btn-lg btn-block">User QR code</button>
        </a>
        <p></p> 

        <a href="{{route('navigation',['actions' => 'user_edit','uid' => base64_encode($uid),'app_url' => base64_encode($app_url),'company' => base64_encode($password) ])}}">
          <button type="button" class="btn btn-light btn-lg btn-block">User Edit</button>
        </a>
        
      </div>
    </div>
</div>
    
@endsection