@extends('app')

@section('content')
<div class="options"> 
    <a href="{{route('navigation',['actions' => 'options','uid' => base64_encode($uid),'app_url' => base64_encode($app_url),'company' => base64_encode($password) ])}}">Back</a>
    
    <div class="card">

      <div class="card-header">
       View Leads 
      </div>
      <div class="card-body">
          @if ($user == 'supervisor')
             <a href="{{route('navigation',['actions' => 'tableE','uid' => base64_encode($uid),'app_url' => base64_encode($app_url),'company' => base64_encode($password) ])}}">
                <button type="button" class="btn btn-secondary btn-lg btn-block">View Exhibitors</button>
            </a>
            <p></p> 
          @endif
           
            <a href="{{route('navigation',['actions' => 'tableS','uid' => base64_encode($uid),'app_url' => base64_encode($app_url),'company' => base64_encode($password) ])}}">
                <button type="button" class="btn btn-primary btn-lg btn-block">Scanned Leads ( {{$count}} )</button>
            </a>
      </div>
    </div>
</div>
    
@endsection