@extends('app')

@section('content')
<div class="options"> 
 
        <a href="{{route('navigation',['actions' => 'options','uid' => base64_encode($uid),'app_url' => base64_encode($app_url),'company' => base64_encode($password) ])}}">Back</a>

<table class="table">
  <thead class="thead-dark">
    <tr>
        @php ($head = ['ID','UID','Device Name','Device ID','Action'])
        @foreach ($head as $user)
          <th scope="col">{{$user}}</th>
        @endforeach
    </tr>
  </thead>
  <tbody>
    @foreach ($device as $key => $item)
    <tr> 
    <th scope="row">{{$key+1}}</th>
        <th>{{$device[$key]->uid}}</th>
        <th>{{$device[$key]->device_name}}</th>
        <th>{{$device[$key]->device_id}}</th>
        <th>   
            <form action="{{ route('removeDevice',['device_id' => $device[$key]->id]) }}" method="GET">
                <button style="width:80px" type="submit" class="btn btn-danger">Remove</button>
            </form>
          
        </th> 
    </tr>
    @endforeach
  </tbody>
</table> 

</div>
</div>
@endsection