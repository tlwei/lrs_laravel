@extends('app')

@section('content')
<div class="options"> 
    <a href="{{route('navigation',['actions' => 'options','uid' => base64_encode($uid),'app_url' => base64_encode($password),'company' => base64_encode($app_url) ])}}">Back</a>
    
    <div class="card">
        
      <div class="card-header">
       User Options
      </div>
      <div class="card-body">
            <a href="{{route('navigation',['actions' => 'password','uid' => base64_encode($uid),'app_url' => base64_encode($app_url),'company' => base64_encode($password) ])}}">
                <button type="button" class="btn btn-secondary btn-lg btn-block">Change Password</button>
            </a>
            <p></p> 
            <a href="{{route('navigation',['actions' => 'picture','uid' => base64_encode($uid),'app_url' => base64_encode($app_url),'company' => base64_encode($password) ])}}">
                <button type="button" class="btn btn-primary btn-lg btn-block">Change Profile Pic</button>
            </a>
      </div>
    </div>
</div>
    
@endsection
