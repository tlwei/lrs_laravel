@extends('admin.app')

@section('content')

    <a href="/option/{{base64_encode($app_url)}}">
        Back
    </a>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table">
        <thead class="thead-dark">
            <tr>
                @php ($users = ['#','Action List','Todo','Action'])
                @foreach ($users as $user)
                  <th scope="col">{{$user}}</th>
                @endforeach
            </tr> 
        </thead>
        <tbody>
            @foreach ($products as $product)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $product->action_list }}</td>
                <td>{{ $product->action }}</td>
                <td>
                    <form action="{{ route('action.destroy',$product->id) }}" method="POST">
                        <input type='hidden' name="product_id" value="{{$product->id}}">
                        {{--  <a class="btn btn-info" href="{{ route('action.show',$product->id) }}">Show</a>  --}}
                        
                        <a class="btn btn-primary" href="{{ route('action.edit', base64_encode($product->id)) }}">Edit</a>
                     
                        @csrf
                        @method('DELETE')
                        
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody> 
    </table>
    {{--  <div class="pull-right">
            <a class="btn btn-success" href="{{ route('products.create') }}"> Create New Product</a>
        </div>  --}}
    {!! $products->links() !!}
    <a href="{{ route('action.create', ['app_url' => base64_encode($app_url)]) }}">
      <button type="button" class="btn btn-secondary btn-lg btn-block">Create ToDo</button>
    </a>
    <p></p> 
      
@endsection