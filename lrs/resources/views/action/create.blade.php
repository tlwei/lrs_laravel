@extends('admin.app')

  
@section('content')
<a href="{{ route('action.index',['app_url' => base64_encode($app_url)]) }}">
    Back
</a>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="/actionList/{{$app_url}}" method="POST">
    @csrf
  
        <div class="form-group">
            <strong>Todo Action:</strong>
            <textarea class="form-control" style="height:150px" name="action" placeholder="Todo Action.."></textarea>
        </div>

        <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>

 
   
</form>
@endsection