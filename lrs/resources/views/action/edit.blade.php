@extends('admin.app')
   
@section('content')
    <a href="{{ route('action.index',['app_url' => base64_encode($product->app_url)]) }}">
        Back
    </a>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {{--  <form action="/actionList/{{$product->app_url}}" method="POST">  --}}
    <form action="/actionList/{{$product->app_url}}" method="POST">
        @csrf
       
        <input type='hidden' name="action_list" value="{{$product->action_list}}">
            <div class="form-group">
                <strong>Action:</strong>
                <textarea class="form-control" style="height:150px" name="action" placeholder="Action..">{{ $product->action }}</textarea>
            </div>

            <button type="submit" class="btn btn-primary btn-lg btn-block">Edit Action</button>

   
    </form>
@endsection