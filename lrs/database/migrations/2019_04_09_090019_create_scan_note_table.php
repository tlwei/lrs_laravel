<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScanNoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scan_note', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uid',250)->nullable();
            $table->string('scan_uid',250)->nullable();
            $table->string('scan_name',250)->nullable();
            $table->string('note_list',4)->nullable();
            $table->string('note',250)->nullable();
            $table->string('app_url',250)->nullable();
            $table->string('email',250)->nullable();
            $table->string('company',250)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scan_note');
    }
}
