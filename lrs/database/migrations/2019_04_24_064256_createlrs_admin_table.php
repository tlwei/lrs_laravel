<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatelrsAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lrs_admin', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('username',30)->nullable();
            $table->string('password',15)->nullable();
            $table->string('name',30)->nullable();
            $table->string('position',20)->nullable();
            $table->string('app_url',20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
