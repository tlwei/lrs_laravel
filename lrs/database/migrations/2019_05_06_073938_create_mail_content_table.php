<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('mail_content', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('app_url',250)->unique()->nullable();
            $table->binary('header_pic1')->nullable();
            $table->binary('header_pic2')->nullable();
            $table->string('date1',50)->nullable();
            $table->string('date2',50)->nullable();
            $table->string('location1',250)->nullable();
            $table->string('location2',250)->nullable();
            $table->binary('content_pic1')->nullable();
            $table->binary('content_pic2')->nullable();
            $table->text('content1')->nullable();
            $table->text('content2')->nullable();
            $table->string('company',250)->nullable();
            $table->string('address',250)->nullable();
            $table->string('email',100)->nullable();
            $table->string('phone',100)->nullable();
            $table->text('facebook')->nullable();
            $table->text('instagram')->nullable();
            $table->text('twitter')->nullable();
            $table->text('LinkedIn')->nullable();
            $table->text('moreContent')->nullable();
            $table->string('CEO',100)->nullable();
            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mail_content');
    }
}
