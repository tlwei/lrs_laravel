<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lrs_users', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('name',250)->nullable();
            $table->string('uid',250)->nullable();
            $table->string('delegate',250)->nullable();
            $table->string('interest',250)->nullable();
            $table->string('company',250)->nullable();
            $table->string('password')->nullable();
            $table->binary('avatar')->nullable();
            $table->string('app_url',250)->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lrs_users');
    }
}
