<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScanUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scan_user', function (Blueprint $table) {
            $table->string('uid')->nullable();
            $table->string('scan_uid',250)->nullable();
            $table->string('name',250)->nullable();
            $table->string('delegate',250)->nullable();
            $table->string('email',250)->nullable();
            $table->string('mobile',250)->nullable();
            $table->string('company',250)->nullable();
            $table->binary('avatar')->nullable();
            $table->string('app_url',250)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scan_user');
    }
}
