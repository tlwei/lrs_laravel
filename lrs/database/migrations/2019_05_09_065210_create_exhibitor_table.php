<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExhibitorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lrs_exhibitor', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uid',250)->nullable();
            $table->string('company',250)->nullable();
            $table->string('title',50)->nullable();
            $table->string('first_name',250)->nullable();
            $table->string('last_name',250)->nullable();
            $table->string('delegate',250)->nullable();
            $table->string('email',100)->nullable();
            $table->string('type',250)->nullable();
            $table->string('mobile',250)->nullable();
            $table->string('source',250)->nullable();
            // $table->string('telephone',250)->nullable();
            // $table->string('address',250)->nullable();
            
            // $table->string('city',250)->nullable();
            // $table->string('zipcode',50)->nullable();
            $table->binary('avatar')->nullable();
            $table->string('app_url',250)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lrs_exhibitor');
    }
}
