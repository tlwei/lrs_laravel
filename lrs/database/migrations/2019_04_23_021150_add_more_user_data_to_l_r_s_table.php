<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreUserDataToLRSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lrs_users', function (Blueprint $table) {
            $table->string('mobile_no',250)->nullable()->after('password');
            $table->string('email',250)->nullable()->after('password');
            $table->string('address',250)->nullable()->after('password');
            $table->string('position',250)->nullable()->after('password');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('mac_address',250)->nullable();
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lrs_users', function (Blueprint $table) {
            //
        });
    }
}
