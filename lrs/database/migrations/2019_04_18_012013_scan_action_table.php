<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ScanActionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scan_action', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uid',250)->nullable();
            $table->string('scan_uid',250)->nullable();
            $table->string('scan_name',25)->nullable();
            $table->string('action',250)->nullable();
            $table->boolean('status')->default(0)->nullable();
            $table->string('app_url',250)->nullable();
            $table->string('email',250)->nullable();
            $table->string('company',250)->nullable();
            $table->string('delegate',250)->nullable();
            $table->string('mobile',250)->nullable();
        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scan_action');
    }
}
