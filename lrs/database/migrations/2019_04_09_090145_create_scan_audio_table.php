<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScanAudioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scan_audio', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uid')->nullable();
            $table->string('scan_uid',250)->nullable();
            $table->string('name',250)->nullable();
            $table->string('audio_list',4)->nullable();
            $table->binary('audio')->nullable();
            $table->string('app_url',250)->nullable();
            $table->timestamps();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scan_audio');
    }
}
