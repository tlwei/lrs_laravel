<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScanTickedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scan_ticked', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('uid')->nullable();
            $table->string('scan_uid',250)->nullable();
            $table->boolean('brochure')->default(0)->nullable();
            $table->boolean('quotation')->default(0)->nullable();
            $table->boolean('sales')->default(0)->nullable();
            $table->boolean('report')->default(0)->nullable();
            $table->boolean('meeting')->default(0)->nullable();
            $table->string('app_url',250)->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scan_ticked');
    }
}
