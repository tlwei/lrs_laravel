<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('app_url',250)->unique()->nullable();
            $table->string('background_color',250)->nullable();
            $table->string('username',250)->nullable();
            $table->string('password',250)->nullable();
            $table->string('font_size',250)->nullable();
            $table->string('list_icon',250)->nullable();
            $table->string('main_title',250)->nullable();
            $table->string('title_color',250)->nullable();
            $table->string('btn_color',250)->nullable();
            $table->string('btn_text',250)->nullable();
            $table->binary('heading_img')->nullable();
            $table->binary('footer_img')->nullable();
            $table->binary('mic_img')->nullable();
            $table->boolean('status')->default(0)->nullable();
            $table->string('user_id',250)->nullable();
            $table->string('start_date',250)->nullable();
            $table->string('end_date',250)->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app');
    }
}
