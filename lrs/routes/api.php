<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//
// Route::post('/checkDate', 'appController@checkDate');
//main app
Route::post('/main', 'appController@mainApp');
//app
Route::post('/app', 'appController@appGenerator');
Route::get('/getApp/{url}', 'appController@getApp');
//user
Route::post('/userLogin', 'userController@userLogin');
//userRegister
Route::post('/userRegister', 'userController@userRegister');
//userRegister
Route::post('/adminRegister', 'userController@adminRegister');
//getUser
Route::post('/scanUser/{uid}', 'scanUserController@scanUser');
//ticked
// Route::post('/ticked/{uid}/{scan_uid}/{app_url}', 'scanTickedController@ticked');
//note
Route::post('/note', 'scanNoteController@note');
//audio
Route::post('/audio', 'scanAudioController@audio');
//actionlist
Route::post('/actionList/{app_url}', 'scanActionListController@actionList');
Route::post('/test', 'scanActionListController@test');

Route::get('/testqr', 'scanActionListController@testqr');

//action
Route::post('/action', 'scanActionController@action');