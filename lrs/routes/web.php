<?php

/* 
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//mail layouts
Route::get('/mailLayout', function () {
    return view('admin.mailContent');
});
Route::get('/', function () {
    return view('welcome');
});
//email content edit

Route::post('/mailContentUpdate', 'ProductController@mailContentUpdate');
//
Route::get('/mailEdit', 'ProductController@mailContentEdit')->name('mailEdit');
//login
Route::get('/user/{url}', function ($url) {
    $url = $url;
    return view('user.login',compact('url'));
})->name('home');

Route::get('/user_admin/{url}', function ($url) {
    $url = $url;
    return view('admin.login',compact('url'));
})->name('user_admin');
 
Route::get('mail', 'webAdminController@mail')->name('mail');
Route::post('webLogin', 'webController@webLogin')->name('login');
Route::post('adminLogin', 'webAdminController@adminLogin')->name('adminLogin');
//navigate
Route::get('/navigation/{navigate}/{uid}/{app_url}/{company}', 'webController@navigation')->name('navigation');
//device
Route::get('removeDevice/{device}', 'webController@removeDevice')->name('removeDevice');
//homeadmin
Route::get('/option/{app_url}', 'webAdminController@admimHome');
//
Route::post('changePassword', 'webController@changePassword')->name('changePassword');
Route::post('changePicture', 'webController@changePicture')->name('changePicture');

//create
Route::post('/userRegister', 'userController@userRegister');
Route::post('/actionList/{app_url}', 'scanActionListController@actionList');
//////crud
Route::resource('products', 'ProductController');
Route::resource('action','ActionController');
////excel
Route::get('export/{app_url}', 'ExcelController@export')->name('export');
Route::get('exportVisitorData/{app_url}', 'ExcelController@exportVisitorData')->name('exportVisitorData');
Route::get('importExportView', 'ExcelController@importExportView')->name('importExportView');
Route::get('importExhibitor', 'ExcelController@importExhibitor')->name('importExhibitor');
Route::post('import/{app_url}', 'ExcelController@import')->name('import');
Route::post('importE/{app_url}', 'ExcelController@importE')->name('importE');

////exhibitorsView
Route::get('exhibitorsView/{app_url}', 'ExcelController@exhibitorsView')->name('exhibitorsView');
// proIndex
Route::get('proIndex/{app_url}', 'ProductController@index')->name('proIndex');
//event
Route::get('event', 'appController@viewAll')->name('viewAll');
//admin
Route::get('/create', function () {
    return view('admin.create_app');
})->name('create');
//expired
Route::post('expired/{id}', 'appController@expired')->name('expired');
//edit view
Route::post('editEventView/{id}', 'appController@editEventView')->name('editEventView');
//edit
Route::post('editEvent/{id}', 'appController@editEvent')->name('editEvent');
