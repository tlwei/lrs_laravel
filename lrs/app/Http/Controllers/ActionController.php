<?php
  
namespace App\Http\Controllers;
  
use App\Product;
use App\ScanActionList;
use Illuminate\Http\Request;
use Alert;

  
class ActionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 
        $app_url = base64_decode($request->app_url);
        $products = ScanActionList::where('app_url',$app_url)->paginate(10);
        // return $products;
        return view('action.index',compact('products','app_url'))
            ->with('i', (request()->input('page', 1) - 1) * 10)->with('app_url',$app_url);
    }
   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $app_url = base64_decode($request->app_url);
        return view('action.create',compact('app_url'));
    }
  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'detail' => 'required',
        ]);
  
        Product::create($request->all());
            
        return redirect()->route('action.index')
                        ->with('success','Product created successfully.');
    }
   
    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('action.show',compact('product'));
    }
   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($request,Product $product)
    {
        $data = ScanActionList::where('id',base64_decode($request))->get();

        $product = $data[0];
        

        return view('action.edit',compact('product'));
    }
  
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {

        $request->validate([
            'name' => 'required',
            'detail' => 'required',
        ]);
  
        $product->update($request->all());
  
        return redirect()->route('action.index')
                        ->with('success','Product updated successfully');
    }
  
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */ 
    public function destroy(Request $request,Product $product)
    {
        ScanActionList::where('id',$request->product_id)->delete();

        Alert::message('User deleted successfully.', "Success");

        return redirect()->back();

    }
}