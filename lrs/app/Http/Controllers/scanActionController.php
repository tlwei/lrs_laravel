<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ScanAction;
use Validator;

class scanActionController extends Controller
{
    //
    public function action(Request $request)
    {
        if(isset($request->status)){
            $status = ['status' => $request->status];
            ScanAction::where('uid', $request->uid)->where('scan_uid', $request->scan_uid)->where('app_url', $request->app_url)->where('action_list', $request->action_list)->update($status);
        }
        // return $request->status;
        $noteData = ScanAction::where('uid', $request->uid)->where('scan_uid', $request->scan_uid)->where('app_url', $request->app_url)->where('deleted_at',NULL)->get();
        return $noteData;
        
        
    }
}
