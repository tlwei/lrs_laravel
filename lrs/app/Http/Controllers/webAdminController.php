<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use Alert;
use \Crypt;
use App\User;
use Illuminate\Support\Facades\Mail;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use App\mailContent;

class webAdminController extends Controller
{
    public function adminLogin(Request $request)
    {   
        $filter = Admin::where('username', $request->user)->get();
        $user = Admin::where('username', $request->user)->where('app_url', $filter[0]->app_url)->get();
        
        $login = Admin::where('username', $request->user)->where('password', $request->password)->get();
        // lrs_main_admin
        if($user == '[]'){
            Alert::message('Account no exist, please try again', " Login Failed");
            return redirect()->back();
        }else{
            if($login == '[]'){
                Alert::message('Password Incorrect, please try again', " Login Failed");
                return redirect()->back();
            }else{
                $uid = $user[0]->username;
                $app_url = $filter[0]->app_url;

                if($uid == "lrs_main_admin"){
                    return view('admin.create_app', compact('uid','app_url'));
                }
                return view('admin.option', compact('uid','app_url'));
            }
        }
    }

    public function admimHome(Request $request,$app_url)
    {
        
        $app_url=base64_decode($app_url);
 
        return view('admin.option', compact('app_url'));

    }

    public function mail(Request $request)
    {
        $userID=base64_decode($request->user);
        $checker=base64_decode($request->mail);
        $app_url=base64_decode($request->app_url);
        $mainContent = mailContent::where('app_url',$app_url)->get();
        
        if($mainContent == '[]'){
            Alert::message("Your Email Content is Empty, Please Fill Up Your Content Before Send!.", "Warning");
            return redirect()->back();    
        }else{
            if($checker == 'single'){
                $user = User::find($userID)->toArray();
                    $user_name=$user['name'];
                    $content=$mainContent[0];
                    $userEmail = $user['email'];
                    Mail::send('admin.mailContent', compact('content','user_name'), function($message) use ($user)
                    {
                        $message->from(env('MAIL_USERNAME'));
                        $message->to($user['email']);
                        $message->subject('Lead Retrieval CMS Mail');
                    });
    
                    Alert::message("Email to $userEmail Sended.", "Success");
                    return redirect()->back();
                    
            
            }else{
                $filter = User::where('app_url',$checker)->get();
                $users=$filter->toArray();
    
                foreach ($users as $key => $user) {
                    $user_name=$user['name'];
                    $content=$mainContent[0];
                    Mail::send('admin.mailContent', compact('content','user_name'), function($message) use ($user) {
                        $message->from(env('MAIL_USERNAME'));
                        $message->to($user['email']);
                        $message->subject('Lead Retrieval CMS Mail');
                    });
                }
                Alert::message("Email to All Users Sended.", "Success");
                return redirect()->back();
            }
        }
        return $request;
        
    }
}
