<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ScanActionList;
use App\ScanAction;
use Validator;
use Alert;
use App\ScanUser;
use App\ScanNote;

class scanActionListController extends Controller
{
    public function testqr()
    {
        $new_data='ACT2019409838,DHARMESH,VIRANI';
        $new_data_to_array=explode(",", $new_data);
        print_r(count($new_data_to_array));
    }
    public function test(Request $request)
    {
        $actions = ScanAction::where('uid',$request->uid)->get();
        $filter = [];
        foreach($actions as $key => $action){
            if($action->status == 1){
                $filter[$action->scan_uid][]=$action->action;
            }
        }
       
        foreach($filter as $key => $action){
            $filter[$key]=implode(", ",$action);
        }

        $notes = ScanNote::where('uid',$request->uid)->get();
        $filterNote = [];
   
        foreach($notes as $key => $action){
            $filterNote[$action->scan_uid][]="Note ".$action->note_list." : ".$action->note;
        }
       
        foreach($filterNote as $key => $action){
            $filterNote[$key]=implode(", ",$action);
        }
        return $filterNote;
        $exports = ScanAction::where('uid',$request->uid)->where('app_url',$request->app_url)->select(['scan_uid','scan_name','company','delegate','email','mobile'])->get();

        foreach($exports as $key => $export){
            $exportData[$export->scan_uid]=$export;
            if($export->scan_uid){
                if(isset($filterNote[$export->scan_uid])){
                    $exportData[$export->scan_uid]->note = $filterNote[$export->scan_uid];
                }else{
                    $exportData[$export->scan_uid]->note = '';
                }
                if(isset($filter[$export->scan_uid])){
                    $exportData[$export->scan_uid]->action = $filter[$export->scan_uid];
                }else{
                    $exportData[$export->scan_uid]->action = '';
                }
            }
        }
        $arr = array();
        
        if($filter !== []){
            foreach ($exportData as $data){
                array_push($arr,$data);
           }
        }
        
        return collect($arr);
       
    }
    //
    public function actionList(Request $request,$app_url)
    //must include
    //app_url
    //action
    //if edit->action_list
    //if delete->delete_status = true,action_list
    {
        
        $data = ScanActionList::where('app_url',$app_url)->withTrashed()->get();
        
        if($request->delete_status == true){
            ScanActionList::where('action_list', $request->action_list)->where('app_url', $app_url)->delete();
            ScanAction::where('action_list', $request->action_list)->where('app_url', $app_url)->delete();
        } 
        
        if(isset($request->action)){
            $validate = [
                'app_url' => $app_url,
                'action' => $request->action,
                'action_list' => count($data)+1
            ];

            $edit = ['action' => $request->action];
            $actionCheck = [];

            if(isset($request->action_list)){
                $actionCheck = ScanActionList::where('action_list', $request->action_list)->where('app_url', $app_url)->get();
            }
            
    
            if($actionCheck == []){
      
                ScanActionList::updateOrCreate($validate);
            }else{
           
                ScanActionList::updateOrCreate(
                    ['action_list' => $request->action_list,'app_url' => $app_url],
                    $edit
                );
                
            }
        }
        $data = ScanActionList::where('app_url',$app_url)->get();

        Alert::message('Action updated successfully.', "Success");
            
        return redirect()->back();

        
    }
}
