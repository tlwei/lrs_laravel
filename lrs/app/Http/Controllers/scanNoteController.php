<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ScanNote;
use Validator;
use App\Exhibitors;

class scanNoteController extends Controller
{
    //delete note -> delete_status = true,note_list
    public function note(Request $request)
    {

    //add scan_name
        $data = ScanNote::where('uid', $request->uid)->where('scan_uid', $request->scan_uid)->where('app_url', $request->app_url)->withTrashed()->get();
        $info = Exhibitors::where('uid', $request->scan_uid)->where('app_url', $request->app_url)->get();

        if(count($info) >= 1){
            // return $info;
            $validate = [
                'uid'=>$request->uid,
                'scan_uid'=>$request->scan_uid,
                'scan_name' =>$request->scan_name,
                'app_url' =>$request->app_url,
                'note_list' =>count($data)+1,
                'company' =>$info[0]->company,
                'email' =>$info[0]->email,
            ];

            if(isset($request->note)){
                $note=[
                    'note'=>$request->note
                ];
                $check = ScanNote::where('scan_uid', $request->scan_uid)->where('uid', $request->uid)->where('note_list', $request->note_list)->where('app_url', $request->app_url)->get();
            
                if($check == '[]'){
                    ScanNote::updateOrCreate($validate,$note);
                }else{

                    ScanNote::updateOrCreate(
                        ['note_list' => $request->note_list,'app_url' => $request->app_url,'scan_uid' => $request->scan_uid],
                        $note
                    );

                }
                // ScanNote::updateOrCreate(
                //     $validate,$note
                // );
            }else if($request->delete_status == true){

                ScanNote::where('note_list',$request->note_list)->where('app_url',$request->app_url)->where('scan_uid',$request->scan_uid)->where('uid',$request->uid)->delete();
            }
        
            $noteData = ScanNote::where('uid', $request->uid)->where('scan_uid', $request->scan_uid)->where('app_url', $request->app_url)->where('deleted_at',NULL)->get();
            return $noteData;
        }else{
            return ['No Exhibitor Exist'];
        }
        
    }
}
