<?php
  
namespace App\Http\Controllers;
  
use App\User;
use Illuminate\Http\Request;
use Alert;
use \Crypt;
use App\mailContent;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$app_url)
    {  

        $app_url = base64_decode($app_url);

        $products = User::where('app_url',$app_url)->select(['id','name', 'uid', 'delegate','interest','company','password','position','address','email','mobile_no','avatar','app_url','mac_address'])->paginate(10);
   
        // return $products;
        return view('products.index',compact('products','app_url'))
            ->with('i', (request()->input('page', 1) - 1) * 10)->with('app_url',$app_url);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $app_url = base64_decode($request->app_url);
        return view('products.create',compact('app_url'));
    }
  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'detail' => 'required',
        ]);
  
        User::create($request->all());
        Alert::message('User created successfully.', "Success");
        return redirect()->back();
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\User  $product
     * @return \Illuminate\Http\Response
     */
    public function show(User $product)
    {
        return view('products.show',compact('product'));
    }
   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($request)
    {
        $id = base64_decode($request);
        $products = User::where('id',$id)->get();
        $product=$products[0];
        return view('products.edit',compact('product'));
    }
  
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $product)
    {
        // return $product;
        $edit=$request->validate([
            'name' => 'required',
            'delegate' => 'required',
            'interest' => 'required',
            'company' => 'required',
            'password' => 'required',
            'position' => 'required',
            'address' => 'required',
            'email' => 'required',
            'mobile_no' => 'required',
            'avatar' => 'image', 
        ]);
        // return $request->file('avatar');
        $product->update($edit);
  
        Alert::message('User updated successfully.', "Success");
            
        return redirect()->back();
    }

    public function mailContentEdit(Request $request)
    {
        $app_url = base64_decode($request->app_url);
   
        $mailContent = mailContent::where('app_url',$app_url)->get();

        return view('products.mailContentEdit',compact('app_url','mailContent'));
    }

    public function mailContentUpdate(Request $request, User $product)
    {
        $app_url = base64_decode($request->app_url);
        
        $edit=$request->validate([
            'header_pic1' => 'required|image',
            'header_pic2' => 'required|image',
            'date1' => 'required',
            'date2' => 'required',
            'location1' => 'required',
            'location2' => 'required',
            'content_pic1' => 'required|image',
            'content_pic2' => 'required|image',
            'content1' => 'required',
            'content2' => 'required',
            'company' => 'required', 
            'address' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'facebook' => 'required', 
            'instagram' => 'required',
            'twitter' => 'required',
            'LinkedIn' => 'required', 
            'moreContent' => 'required',
            'CEO' => 'required', 
        ]);
        
        $images = ['header_pic1','header_pic2','content_pic1','content_pic2'];
        foreach ($images as $image) {
            $path = $request->file($image)->store(
                "/appId_{$app_url}",
                'admin'
             );
            
            #get storage path and put in array
            $filename = "/appId_{$app_url}/email_images/{$image}.jpg";
            $edit[$image] = $filename;
            if(Storage::disk('admin')->exists($filename)) {
                Storage::disk('admin')->delete($filename);
            }
            Storage::disk('admin')->move($path, $filename);
        }
        // return $request->file('avatar');
        mailContent::updateOrCreate(
            ['app_url' => $app_url],
            $edit
        );
  
        Alert::message('Mail Content Updated!', "Success");
            
        return redirect()->back();
    }
  
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $product)
    {
        $product->delete();
        Alert::message('User deleted successfully.', "Success");

        return redirect()->back();
    }
}