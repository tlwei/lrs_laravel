<?php

namespace App\Http\Controllers;
use App\App;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Admin;
use Alert;

class appController extends Controller
{
    
    public function mainApp(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'app_url' => 'required',
            'background_color' => 'required',
            'font_size' => 'required',
            'list_icon' => 'required',
            'main_title' => 'required',
            'title_color'=>'required',
            'btn_color' => 'required',
            'btn_text' => 'required',
            
        ])->validate();

        $img_validate = Validator::make($request->all(), [
            'heading_img' => 'required|image',
            'footer_img' => 'required|image',
            'mic_img' => 'required|image',
        ])->validate();

        $images = ['heading_img','footer_img','mic_img'];
        foreach ($images as $image) {
            $path = $request->file($image)->store(
                "/appId_{$request->app_url}",
                'admin'
             );
            
            #get storage path and put in array
            $filename = "/appId_{$request->app_url}/profile/{$image}.jpg";
            $img_validate[$image] = $filename;
            if(Storage::disk('admin')->exists($filename)) {
                Storage::disk('admin')->delete($filename);
            }
            Storage::disk('admin')->move($path, $filename);
        }
        
        App::updateOrCreate($img_validate,$validate);
        return $validate;
    }

    public function appGenerator(Request $request)
    {
        
        $validate = [
            'app_url' => $request->app_url,
            'background_color' => $request->background_color,
            'font_size' => $request->font_size,
            'list_icon' => $request->list_icon,
            'main_title' => $request->main_title,
            'title_color'=>$request->title_color,
            'btn_color' => $request->btn_color,
            'btn_text' => $request->btn_text,
            'user_id' => $request->user_id,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ];
        ///img

        $path = $request->file('footer_img')->store(
            "/appId_{$request->app_url}",
            'admin'
         );
        
        #get storage path and put in array
        $filename = "/appId_{$request->app_url}/profile/footer_img.jpg";
        $img_validate['footer_img'] = $filename;
        if(Storage::disk('admin')->exists($filename)) {
            Storage::disk('admin')->delete($filename);
        }
        Storage::disk('admin')->move($path, $filename);
    
        
        ////
        
        
        ////
        $get = App::where('user_id',$request->user_id)->get();
        
        if(count($get) == 1){
            return "!!!Exhibitor ID Short Form Exist Plase Use Another One.!!!";
        }else{
            $random_password = rand ( 10000 , 99999 );
            $admin_user = [
                'username' => $request->user_id.'admin',
                'password' => $random_password,
                'name' => 'admin',
                'position' => 'admin',
                'app_url' => $request->app_url,
            ];
            $app_user = [
                'username' => $request->user_id.'admin',
                'password' => $random_password,
            ];
            ///
            App::updateOrCreate(['app_url' => $request->app_url],$img_validate);
            App::updateOrCreate(['app_url' => $request->app_url], $validate);
            App::updateOrCreate(['app_url' => $request->app_url], $app_user);
            Admin::updateOrCreate(['app_url' => $request->app_url], $admin_user);
            
            echo '|------------------------------------------NOTIFICATION----------------------------------------------|'."<br>";
            echo '|----------------------------------------------------------------------------------------------------|'."<br>";
            echo '|Exhibition Event Successful Created-----------------------------------------------------------------|'."<br>";
            echo '|----------------------------------------------------------------------------------------------------|'."<br>";
            echo '|This is your admin ID : '.$request->user_id.'admin--------------------------------------------------|'."<br>";
            echo '|----------------------------------------------------------------------------------------------------|'."<br>";
            echo '|Your PASSWORD is '.$random_password.'---------------------------------------------------------------|'."<br>";
            echo '|----------------------------------------------------------------------------------------------------|'."<br>";
            echo '|:::WARNING::: please keep your !!PASSWORD!! --------------------------------------------------------|'."<br>";
            echo '|----------------------------------------------------------------------------------------------------|'."<br>";
            echo '|You can get your PASSWORD and ID again at main admin panel------------------------------------------|'."<br>";
            echo '|----------------------------------------------------------------------------------------------------|'."<br>";


        }

        // $img_validate = Validator::make($request->all(), [
        //     'heading_img' => 'required|image',
        //     'footer_img' => 'required|image',
        //     'mic_img' => 'required|image',
        // ])->validate();

        // $images = ['heading_img','footer_img','mic_img'];
        // foreach ($images as $image) {
        //     $path = $request->file($image)->store(
        //         "/appId_{$request->app_url}",
        //         'admin'
        //      );
            
        //     #get storage path and put in array
        //     $filename = "/appId_{$request->app_url}/profile/{$image}.jpg";
        //     $img_validate[$image] = $filename;
        //     if(Storage::disk('admin')->exists($filename)) {
        //         Storage::disk('admin')->delete($filename);
        //     }
        //     Storage::disk('admin')->move($path, $filename);
        // }
        // App::updateOrCreate($img_validate,$validate);
        
        /// lrs admin user ID

        
    }
    //edit evnet
    public function editEvent(Request $request,$id)
    {
        $validate = [
            'app_url' => $request->app_url,
            'background_color' => $request->background_color,
            'font_size' => $request->font_size,
            'list_icon' => $request->list_icon,
            'main_title' => $request->main_title,
            'title_color'=>$request->title_color,
            'btn_color' => $request->btn_color,
            'btn_text' => $request->btn_text,
            'user_id' => $request->user_id,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ];
        ///img
        if($request->file('footer_img')){
            $path = $request->file('footer_img')->store(
                "/appId_{$request->app_url}",
                'admin'
             );
            
            #get storage path and put in array
            $filename = "/appId_{$request->app_url}/profile/footer_img.jpg";
            $img_validate['footer_img'] = $filename;
            if(Storage::disk('admin')->exists($filename)) {
                Storage::disk('admin')->delete($filename);
            }
            Storage::disk('admin')->move($path, $filename);

            App::updateOrCreate(['app_url' => $request->app_url],$img_validate);
        }
        
        App::updateOrCreate(['app_url' => $request->app_url],$validate);

        Alert::message('Exhibition Event Changed.', "Success");
        
        return view('admin.create_app');
   
    }
    //
    public function editEventView(Request $request,$id)
    {
        $id = base64_decode($id);
        
        $products = App::where('id',$id)->select(['id','app_url', 'background_color', 'font_size','list_icon','main_title','title_color','btn_color','btn_text','footer_img','user_id','start_date','end_date','expired_status'])->get();
        $product=$products[0];
        return view('exhibition.edit',compact('product'));

 
    }

    //expired
    public function expired(Request $request,$id)
    {
        $id = base64_decode($id);

        $status = App::where('id',$id)->select('expired_status')->get();

        if($status[0]->expired_status == 1){
            App::where('id',$id)->update(['expired_status' => 0]);
            Alert::message('Event On Processing.', "Success");
        }else{
            App::where('id',$id)->update(['expired_status' => 1]);
            Alert::message('Event Expired.', "Success");
            
        }
        
        return redirect()->back();
    }
    //
    //view
    public function viewAll(Request $request)
    {
        $products = App::select(['id','app_url','username','password', 'background_color', 'font_size','list_icon','main_title','title_color','btn_color','btn_text','footer_img','user_id','expired_status'])->paginate(10);
        
        // return $products;
        return view('exhibition.index',compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }
    //
    public function getApp(Request $request,$url)
    {
        $appData = App::where('app_url',$url)->get();
        return $appData;
    }
}
