<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use Validator;
use App\ScanAudio;
use ZipArchive;
use Alert;
use App\User;
use App\Exhibitors;

class scanAudioController extends Controller
{
    public function audio(Request $request)
    {
        //uid scan_uid app_url audio_list name
        if($request->uid == 'TESTER'){
            $request->app_url = 'tes';

        }

        $data = ScanAudio::where('uid', $request->uid)->where('scan_uid', $request->scan_uid)->where('app_url', $request->app_url)->withTrashed()->get();
        $name = count($data)+1;
        $validate = [
            'uid'=>$request->uid,
            'scan_uid'=>$request->scan_uid,
            'app_url' => $request->app_url,
            'audio_list' => count($data)+1,
            'name' => "audio_$name",
        ];
        $usr = Exhibitors::where('uid',$request->scan_uid)->where('app_url',$request->app_url)->select(['first_name','last_name','company'])->get();
        if(count($usr) >= 1){
            $user_details = $usr[0];
        }else{
            $user_details = '';
        }
        
        if(isset($request->audio)){
            $audio_validate = Validator::make($request->all(), [
                'audio' => 'required',
            ])->validate();

            $audios = ["audio"];

            foreach ($audios as $audio) {
                $path = $request->file($audio)->store(
                    "/$request->app_url/userId_{$request->uid}",
                    'admin'
                 );
             
                #get storage path and put in array
                
                $file = "/$request->app_url/userId_{$request->uid}/{$user_details->first_name}_{$user_details->last_name}_{$user_details->company}/audio/audio_$request->audio_list.acc";
                $filename = str_replace(" ", "_", $file);
                
                $audio_validate[$audio] = $filename;
                if(Storage::disk('admin')->exists($filename)) {
                    Storage::disk('admin')->delete($filename);
                }
        
                Storage::disk('admin')->move($path, $filename);
            }
            ScanAudio::updateOrCreate($audio_validate,$validate);
        }else if($request->delete_status == true){
            $file = "/$request->app_url/userId_{$request->uid}/{$user_details->first_name}_{$user_details->last_name}_{$user_details->company}/audio/audio_$request->audio_list.acc";
            $filename = str_replace(" ", "_", $file);

            if(Storage::disk('admin')->exists($filename)) {
                Storage::disk('admin')->delete($filename);
            }
            ScanAudio::where('audio_list',$request->audio_list)->where('app_url',$request->app_url)->where('scan_uid',$request->scan_uid)->where('uid',$request->uid)->delete();
        }

        $audioData = ScanAudio::where('uid', $request->uid)->where('scan_uid', $request->scan_uid)->where('app_url', $request->app_url)->where('deleted_at',NULL)->get();

        return $audioData;
        
    }
 
    public function download($uid,$app_url)
    {
        
        $uid=$uid;
        $app_url=$app_url;
        // return response()->download(public_path("app/UserID_2/" . $filename));
        $zip_file = 'audio.zip';
        $zip = new \ZipArchive();

        if (file_exists(public_path("/app/$app_url/userId_$uid/"))) {
            
            $zip->open($zip_file, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
            //public_path('/app/app_url_name/user_id/');
            $path = public_path("/app/$app_url/userId_$uid/");
            
            $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path));
            foreach ($files as $name => $file)
            {
                // We're skipping all subfolders
                if (!$file->isDir()) {
                    $filePath     = $file->getRealPath();
                    // extracting filename with substr/strlen
                    $relativePath = 'user_audio/' . substr($filePath, strlen($path) + 0);
                
                    $zip->addFile($filePath, $relativePath);
                }
            }
            $zip->close();
            return response()->download($zip_file);
        }else{
            Alert::message('No audio resources found.', " Download Failed");
            return redirect()->back();
        }
    }
}
