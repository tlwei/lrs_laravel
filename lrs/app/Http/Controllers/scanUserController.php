<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ScanUser;
use App\ScanAudio;
// use App\ScanTicked;
use App\ScanNote;
use App\ScanAction;
use App\ScanActionList;
use App\User;
use App\Exhibitors;

class scanUserController extends Controller
{
    public function scanUser(Request $request,$uid)
    {   
        
        //must inclue >> app_url,scan_uid
        $user = $request->scan_uid;
        $app_url = $request->app_url;

        $scanUser = Exhibitors::where('uid', $user)->where('app_url', $app_url)->get();
        
        if($scanUser == '[]'){
            // return ["User$user no exist"];

            $new_data_to_array=explode(",", $user);
            if(count($new_data_to_array) == 3){
                $id=$new_data_to_array[0];
                

                $duplicate = ScanUser::where('uid', $uid)->where('scan_uid', $id)->where('app_url', $app_url)->get();
                
                if(count($duplicate) !== 0){
                    return ["User scanned success."];
                }else{
                    $scanUser=[
                        [
                            'title' => NULL,
                            'first_name'=>mb_substr($new_data_to_array[1], null, null, 'UTF-8'),
                            'last_name'=>mb_substr($new_data_to_array[2], null, null, 'UTF-8'),
                            'uid'=> $new_data_to_array[0],
                            'delegate'=> NULL,
                            'company' => NULL,
                            'avatar' => NULL,
                            'app_url'=>$app_url,
                            'email'=>NULL,
                            'mobile'=>NULL
                        ]
                    ];
                    Exhibitors::updateOrCreate(
                        ['uid' => $new_data_to_array[0]],
                        [
                            'uid' => $new_data_to_array[0],
                            'company' => " ",
                            'title' => NULL,
                            'first_name' => mb_substr($new_data_to_array[1], null, null, 'UTF-8'),
                            'last_name' => mb_substr($new_data_to_array[2], null, null, 'UTF-8'),
                            'delegate' => NULL,
                            'email' => NULL,
                            'type' => NULL,
                            'mobile' => NULL,
                            'source' => NULL,
                            'app_url' => $app_url,
                       ]);
                    
                }

            }else{
                return ["Scanned qrcode format incorrect."];
            }

        }
            //uid name delegate email company avatar
            $data = $scanUser[0];
    
            if(is_array($scanUser[0]) ){
                $data = (object)$data;
            }
            

    
            $validate=[
                'uid' => $uid,
                'name' => $data->title." ".$data->first_name." ".$data->last_name,
                'scan_uid' => $data->uid,
                'delegate' => $data->delegate,
                'company' => $data->company,
                'avatar' => $data->avatar,
                'app_url'=> $app_url,
                'email'=> $data->email,
                'mobile'=> $data->mobile
     
            ];
            
            $duplicate = ScanUser::where('uid', $uid)->where('scan_uid', $user)->where('app_url', $app_url)->get();
            $count = count($duplicate)+1;
            if($count !== 1 &&  $user == 'ACT2019409838'){
                return ["User $user scanned success."];
            }
            ScanUser::create(
                $validate
            );
   
            $actionList = ScanActionList::where('app_url',$app_url)->get();

            foreach($actionList as $actionValue){

                $action = [
                    'uid' => $uid,
                    'scan_uid' => $data->uid,
                    'scan_name' => $data->title." ".$data->first_name." ".$data->last_name,
                    'app_url'=> $app_url,
                    'action_list' => $actionValue->action_list,
                    'action' => $actionValue->action,
                    'company' => $data->company,
                    'email' => $data->email,
                    'delegate' => $data->delegate,
                    'mobile'=> $data->mobile
                    
                ];

                ScanAction::updateOrCreate($action);
            }
            if($count == 1){
                return ["User scanned success. Scanned times: $count."];
            }else if($duplicate !== '[]'){
                return ["User is a duplicate scan ID, User scanned times : $count."];
            }

            return $validate;
        
    }
}