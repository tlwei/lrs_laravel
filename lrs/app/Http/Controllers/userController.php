<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ScanUser;
use App\ScanAudio;
use App\ScanTicked;
use App\ScanAction;
use App\ScanNote;
use App\User;
use Alert;
use \Crypt;
use Validator;
use Illuminate\Support\Facades\Storage;
use App\Admin;
use App\App;
use App\DeviceID;

class userController extends Controller
{
    public function userLogin(Request $request)
    {   
       
        if($request->user !== NULL){
            $user = User::where('uid',$request->user)->get();
            
            if($user == '[]'){
                return ["Account does not exist."];
            }
            
        }else{
            return ["Account does not exist."];
        }
        
        if($user[0]->app_url){
            $app = App::where('app_url', $user[0]->app_url)->get();
     
            if($app[0]->expired_status == 1){
                return ["This is an expired event account, you could get your collected data on our website."];
            }else{
                
                $user = User::where('uid', $request->user)->where('app_url', $user[0]->app_url)->get();

                $login = User::where('uid', $request->user)->where('password', $request->password)->get();
                
                if($user == '[]'){
                    return ["Account does not exist."];
                }else{
                    if($login == '[]'){
                        return ["$request->user password incorrect."];
                    }else{
                        $value = [
                            'uid' => $request->user,
                            'device_id' => $request->device_id,
                            'device_name'=> $request->device_name,
                            'app_url' => $login[0]->app_url,
                        ];
                        $count = DeviceID::where('uid',$request->user)->get();
                        $check_device_id = DeviceID::where('device_id',$request->device_id)->get();
                       
                        
                        if(count($count) > 3 && $check_device_id == '[]'){
                            return ["Account has exceeded login Device ID (Maximum Login is 4 devices, visit website to clear Device ID)."];
                        }else{
                            
                            DeviceID::updateOrCreate(
                                ['device_id' => $request->device_id,'app_url' => $login[0]->app_url],
                                $value
                            ); 

                            User::where('uid', $request->user)
                            ->where('app_url', $login[0]->app_url)
                            ->update(['mac_address' => $request->mac_address]);
                            
                            
                            $ScanUser = ScanUser::where('uid', $request->user)->orderBy('updated_at','DESC')->get();

                 
                            $ScanAudio = ScanAudio::where('uid', $request->user)->get();
                            $ScanTicked = ScanAction::where('uid', $request->user)->get();

                            $ScanNote = ScanNote::where('uid', $request->user)->get();

                            $userAction = array();

                            $sortAction = [[$ScanTicked,'ticked'],[$ScanNote,'note'],[$ScanAudio,'audio']];

                            foreach ($sortAction as $element) {
                                foreach ($element[0] as $details) {
                                    $userAction[$details['scan_uid']][$element[1]][] = $details;
                                }
                            }

                            $login[0]->scanUser=$ScanUser;
                        
                            foreach ($login[0]->scanUser as $key => $value) {
                            
                                if(isset($userAction[$value->scan_uid])){
                                    $value->userAction=$userAction[$value->scan_uid];
                                }

                            }
                        
                            return $login;
                        }
                    }
                }
            }
        }else{
            return ["Account does not exist."];
        }
        
    }

    public function generateUID($app_url)  
    {
        $user_id = App::where('app_url',$app_url)->get();
        $id = $user_id[0]->user_id;
        $uid = $id.rand ( 10000 , 99999 );
        $res = User::where('uid',$uid)->get();
        while ($res == '[]') {
            return $uid;
        }
    }

    ///users update or create
    public function userRegister(Request $request)
    {

        $validate = Validator::make($request->all(), [
            'name' => 'required',
            'delegate' => 'required',
            'interest' => 'required',
            'company' => 'required',
            'password' => 'required',
            'position' => 'required',
            'address' => 'required',
            'email' => 'required',
            'mobile_no' => 'required',
            'avatar' => 'image', 
            'uid'=>'required',
            'app_url' => 'required',
        ])->validate();
        
       if(isset($request->status)){
            echo 'edit';
       }else{
            $validate['app_url'] = base64_decode($request->app_url);
       }    
        
        if($validate['uid'] == 'new'){
            $validate['uid'] = $this->generateUID(base64_decode($request->app_url));
        }
        
        if($request->file('avatar') == ''){
            echo 'no_img';
        }else{
            $images = ['avatar'];
            foreach ($images as $image) {
            $path = $request->file($image)->store(
                "/User_{$request->uid}",
                'admin'
             );
            
            #get storage path and put in array
            $filename = "/{$request->app_url}/User_{$request->uid}/{$image}.jpg";
            $validate[$image] = $filename;
            if(Storage::disk('admin')->exists($filename)) {
                Storage::disk('admin')->delete($filename);
            }
            Storage::disk('admin')->move($path, $filename);
        }
        }
        

        User::updateOrCreate(
            ['uid'=>$request->uid,'app_url'=>$request->app_url],
            $validate
        );

        Alert::message('User updated successfully.', "Success");
            
        return redirect()->back();

        // return $validate;
    }

    public function adminRegister(Request $request)
    {

        $validate = Validator::make($request->all(), [
            'name'=>'required',
            'username'=>'required',
            'app_url' => 'required',
            'position' => 'required',
            'password' => 'required',
        ])->validate();

        Admin::updateOrCreate(
            ['username'=>$request->username,'app_url'=>$request->app_url],
            $validate
        );

        return $validate;
    }
}

