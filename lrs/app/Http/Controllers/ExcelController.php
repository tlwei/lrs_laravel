<?php
   
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use App\Imports\ExhibitorsImport;
use App\Exports\ScanNoteExport;
use App\Exports\ScanActionExport;
use App\Exports\exhibitorExport;
use App\Exports\SelfExport;
use App\Exports\ScanUserExport;
use Validator;

use App\ScanNote;
use Maatwebsite\Excel\Facades\Excel;
use Alert;
use App\ScanAction;
use \Crypt;
use App\ScanUser;
use App\User;
use App\Exhibitors;
use App\Exports\exportVisitor;

class ExcelController extends Controller
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function importExportView(Request $request)
    {

       $app_url=base64_decode($request->app_url);

       return view('import',compact('app_url'));
    }

    public function importExhibitor(Request $request)
    {

       $app_url=base64_decode($request->app_url);

       return view('exhibitorImport',compact('app_url'));
    }
   
    
    /**
    * @return \Illuminate\Support\Collection
    */
    public function export($app_url)  
    {
        return Excel::download(new UsersExport($app_url), 'UsersData.xlsx');
    }

    public function exportVisitorData($app_url)  
    {
        return Excel::download(new exportVisitor($app_url), 'VisitorsData.xlsx');
    }
   
    /**
    * @return \Illuminate\Support\Collection
    */

    public function import($app_url,Request $request) 
    {
        // "EX".rand ( 00001 , 99999 );

        if($request->file){
            Excel::import(new UsersImport($app_url),request()->file('file'));
            Alert::message('importing users..', " Import Tips");
        }else{
            Alert::message('No resource found..', " Import Tips");
        }
        
        return back();
    }

    

    public function importE($app_url,Request $request) 
    {
        if($request->file){
            Excel::import(new ExhibitorsImport($app_url),request()->file('file'));
            Alert::message('importing exhibitors..', " Import Tips");
        }else{
            Alert::message('No resource found..', " Import Tips");
        }
        // "EX".rand ( 00001 , 99999 );
        
        return back();
    }

    public function exhibitorsView($app_url) 
    {
        $app_url = base64_decode($app_url);
        $products = Exhibitors::where('app_url',$app_url)->paginate(10);
   
        return view('exhibitorsView',compact('products','app_url'))
            ->with('i', (request()->input('page', 1) - 1) * 10)->with('app_url',$app_url);
    }

    //export notes
    public function exportNote($uid,$app_url) 
    {
        
        $check = ScanNote::where('uid',$uid)->where('app_url',$app_url)->get(); 
        if($check == '[]'){
            Alert::message('No notes resources found.', " Download Failed");
            return redirect()->back();
        }else{
            return Excel::download(new ScanNoteExport($uid,$app_url), 'Notes.xlsx');
        } 
        
    }

    public function exportActions($uid,$app_url) 
    {
        
        $check = ScanAction::where('uid',$uid)->where('app_url',$app_url)->get();
        
        if($check == '[]'){
            Alert::message('No resources found.', " Download Failed");
            return redirect()->back();
        }else{
            return Excel::download(new ScanActionExport($uid,$app_url), 'ScannedUserAndData.xlsx');
        }
        
    }

    public function exportExhibitor($uid,$app_url,$company) 
    { 
        return Excel::download(new exhibitorExport($uid,$app_url,$company), 'Exhibitors.xlsx');

    }

    public function exportSelf($uid,$app_url) 
    {
        return Excel::download(new SelfExport($uid,$app_url), 'SelfData.xlsx');
        
    }
    
    public function exportScanned($uid,$app_url) 
    {
       
        $scannedUser = ScanUser::where('uid',$uid)->where('app_url',$app_url)->get();
        
        if($scannedUser == '[]'){
            Alert::message('No scanned users found.', " Download Failed");
            return redirect()->back();
        }else{
            return Excel::download(new ScanUserExport($uid,$app_url), 'ScannedUserData.xlsx');
        }
        
        
    }

}