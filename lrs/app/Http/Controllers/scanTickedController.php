<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ScanTicked;
use Validator;
class scanTickedController extends Controller
{ 
    //
    public function ticked(Request $request,$uid,$scan_uid,$app_url)
    {

        $validate = Validator::make($request->all(), [
            'brochure'=>'required',
            'quotation'=>'required',
            'sales' => 'required',
            'report' => 'required',
            'meeting' => 'required',
        ])->validate();

        ScanTicked::where('uid', $uid)->where('scan_uid', $scan_uid)->where('app_url', $app_url)->update($validate);
        return $validate;
        
        
    }
}
