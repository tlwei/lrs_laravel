<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\ScanUser;
use Alert;
use \Crypt;
use \QrCode;
use Illuminate\Support\Facades\Storage;
use App\DeviceID;

class webController extends Controller
{
    public function webLogin(Request $request)
    {   
        if($request->user == 'TESTER'){
            $request->app_url = 'DEMO'; 
        }
        $user = User::where('uid', $request->user)->get();
        $login = User::where('uid', $request->user)->where('password', $request->password)->get();
        
        if($user == '[]'){
            Alert::message('Account no exist, please try again', " Login Failed");
            return redirect()->back();
        }else{
            if($login == '[]'){
                Alert::message('Password Incorrect, please try again', " Login Failed");
                return redirect()->back();
            }else{
               
                $uid = $user[0]->uid;
                $app_url = $user[0]->app_url;
                $password =$request->password;
                
                return view('content.options', compact('uid','app_url','password'));
            }
        }
    }

    public function leads($uid,$app_url)
    {   
        $uid=base64_decode($uid); 
        $app_url=base64_decode($app_url);
        return view('content.leads',compact('uid','app_url'));
    }

    public function navigation($navigate,$uid,$app_url,$company)
    {   
        
    
        $excel = new \App\Http\Controllers\ExcelController();
        $audio = new \App\Http\Controllers\scanAudioController();

        $uid=base64_decode($uid);
        $app_url=base64_decode($app_url);
        $company=base64_decode($company);
        $password = $company;

        if($uid == 'TESTER'){
            $app_url = 'DEMO';
        }
        switch ($navigate) {
            case 'data':
                return view('content.data',compact('uid','app_url','password'));
                break;
            case 'leads':
                $count = count(ScanUser::where('uid', $uid)->where('app_url', $app_url)->get());
                $permission = User::where('uid', $uid)
                ->where('app_url', $app_url)
                ->select('position')->get();
                $user = $permission[0]->position;
                return view('content.leads',compact('uid','app_url','password','count','user'));
                break;
            case 'options':
                return view('content.options',compact('uid','app_url','password'));
                break;
            case 'tableE':
                return $this->tableE($uid,$app_url,$password);
                // return view('content.tableE',compact('uid','app_url'));
                break;
            case 'tableS':
                return $this->tableS($uid,$app_url,$password);
                // return view('content.tableS',compact('uid','app_url'));
                break;
            case 'exportNote':
                return $excel->exportNote($uid,$app_url);
                break;
            case 'exportActions':
                return $excel->exportActions($uid,$app_url);
                break;
            case 'download':
                return $audio->download($uid,$app_url);
                break;
            case 'exportExhibitor':
                return $excel->exportExhibitor($uid,$app_url,$company);
                break;
            case 'exportSelf':
                return $excel->exportSelf($uid,$app_url);
                break;
            case 'exportScanned':
                return $excel->exportScanned($uid,$app_url);
                break;
            case 'qrcode':
                return $this->qrcode($uid,$company,$app_url);
                break; 
            case 'user_edit':
                return $this->userEdit($uid,$app_url,$company);
                break;
            case 'password':
                return $this->password($uid,$company,$app_url);
                break;
            case 'picture':
                return $this->picture($uid,$company,$app_url);
                break;
            case 'device':
                return $this->deviceID($uid,$app_url,$password);
                
                break;
            default:
                # code...
                break;
        }
    }

    public function deviceID($uid,$app_url,$password){
        $uid=$uid;
        $app_url=$app_url;
        $password=$password;
        $device=DeviceID::where('uid',$uid)->where('app_url',$app_url)->get();
        return view('content.device',compact('device','uid','app_url','password'));
    }

    public function removeDevice($device_id){
        DeviceID::where('id',$device_id)->delete();
        Alert::message('Remove device ID successfully.', "Success");
        return redirect()->back();
    }

    public function tableE($uid,$app_url,$password)
    { 
        $uid=$uid;
        $app_url=$app_url;
        $password=$password;
        $exhibitor =[];
        $user = User::where('uid', $uid)->get();
        
        $self = User::where('uid', $uid)
            ->where('app_url', $user[0]->app_url)
            ->select(['name', 'uid','delegate','email','company','mobile_no','email','address','position'])->get();
        if($self[0]->position == 'supervisor'){ 
            $exhibitor = User::where('company', $self[0]->company)->where('position', 'exhibitor')->where('app_url', $user[0]->app_url)
                ->select(['name', 'uid','delegate','email','company','mobile_no','email','address','position'])->get();
            return view('content.tableE',compact('self','exhibitor','uid','app_url','password'));
        }else{
            return view('content.tableE',compact('self','exhibitor','uid','app_url','password'));
        }
    }

    public function tableS($uid,$app_url,$password)
    { 

        $password=$password;
        $uid=$uid;
        $app_url=$app_url;
        $exhibitor =[];
        $scannedUser = ScanUser::where('uid', $uid)
            ->where('app_url', $app_url)
            ->select(['scan_uid', 'name', 'delegate','email','company','mobile'])->get();

        return view('content.tableS',compact('scannedUser','uid','app_url','password'));

    }

    public function qrcode($uid,$password,$app_url)
    {
        
        $app_url=$app_url;
        $qrcodeValue = $uid."&UserQrcodeLogin=".$password;
        return view('qrCode',compact('qrcodeValue','app_url','uid','password'));
    }

    public function userEdit($uid,$password,$app_url)
    {
        
        $uid=$uid;
        $app_url=$app_url;
        $password=$password;
        return view('content.user',compact('uid','app_url','password'));
    }

    public function password($uid,$password,$app_url)
    {
        $uid=$uid;
        $app_url=$app_url;

        return view('content.password',compact('uid','app_url','password'));
    }

    public function picture($uid,$password,$app_url)
    {
        $uid=$uid;
        $app_url=$app_url;
        $password=$password;
        
        // return $uid.$password.$app_url;
        return view('content.picture',compact('uid','app_url','password'));
    }

    public function changePassword(Request $request)
    {
        $uid=$request->uid;
        $url=$request->app_url;
        // return $request;
        if($request->password !== $request->old_password){
            Alert::message('Old Password is Incorrect', " Password Tips");
            return redirect()->back();
        }else{
            if($request->new_password == '' || $request->new_password !== $request->confirm_password){
                Alert::message('New Password and Confirm Password is Different', " Password Tips");
                return redirect()->back();
            }else{
                $confirm=['password'=>$request->new_password];
                User::where('uid', $uid)->where('app_url', $url)->update($confirm);

                Alert::message('Password Changed! Please Login Again', " Success Tips");
                return view('user.login',compact('url'));
            }
        }
        return $request;
        

        return view('content.picture',compact('uid','app_url','password'));
    }

    public function changePicture(Request $request)
    {
        
        $validate = \Validator::make($request->all(), [
            'avatar' => 'image', 
            'uid'=>'required',
            'app_url' => 'required',
        ])->validate();
        
        if($request->file('avatar') == ''){
            Alert::message('No Image Selected.', " Tips");
            return redirect()->back();
        }else{
            $images = ['avatar'];
            foreach ($images as $image) {
                $path = $request->file($image)->store(
                    "/User_{$request->uid}",
                    'admin'
                 );
             
                #get storage path and put in array
                $filename = "/{$request->app_url}/User_{$request->uid}/{$image}.jpg";
                $validate[$image] = $filename;
                if(Storage::disk('admin')->exists($filename)) {
                    Storage::disk('admin')->delete($filename);
                }
                Storage::disk('admin')->move($path, $filename);
                User::where('uid', $request->uid)->where('app_url', $request->app_url)->update($validate);
                
            }
            Alert::message('Profile Picture Saved.', " Tips");
            return redirect()->back();
        }

    }
}
