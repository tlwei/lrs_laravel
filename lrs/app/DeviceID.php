<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeviceID extends Model
{
    protected $table = 'device_id';

    protected $fillable = [
        'uid','device_id','device_name','app_url'
    ]; 
}