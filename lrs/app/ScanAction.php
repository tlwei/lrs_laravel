<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; // <-- This is required

class ScanAction extends Model
{
    //
    use SoftDeletes;
    
    protected $table = 'scan_action';

    protected $fillable = [
        'uid', 'scan_action','scan_name','action','app_url','status','action_list','scan_uid','company','email','delegate','mobile'
    ];
}
