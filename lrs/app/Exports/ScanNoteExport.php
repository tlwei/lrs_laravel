<?php

namespace App\Exports;

use App\ScanNote;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings; 


class ScanNoteExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $uid,$app_url;

    public function __construct(String $uid,String $app_url)
    {
        $this->uid = $uid;
        $this->app_url = $app_url;
    }

    public function collection()
    { 
        return ScanNote::where('uid',$this->uid)->where('app_url',$this->app_url)->select(['note_list', 'scan_name', 'note','email','company'])->get();
    }
 
    public function headings(): array
    {
        return [
            'Note List',
            'Username',
            'Note Content',
            'Email',
            'Company'
        // etc


        ];
    }
}
