<?php

namespace App\Exports;

use App\ScanUser;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings; 

class ScanUserExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $uid,$app_url;

    public function __construct(String $uid,String $app_url)
    {
        $this->uid = $uid;
        $this->app_url = $app_url;
    }

    public function collection()
    {
        return ScanUser::where('uid',$this->uid)->where('app_url',$this->app_url)->select(['scan_uid','name', 'delegate','company','email','mobile'])->get();
    }

    public function headings(): array
    {
        return [
            'SRN (UID)',
            'Name',
            'Job Title',
            'Company',
            'Mobile',
            'Email',
        // etc


        ];
    }
}
