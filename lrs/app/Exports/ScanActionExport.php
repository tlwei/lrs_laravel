<?php

namespace App\Exports;

use App\ScanAction;
use App\ScanNote;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings; 

class ScanActionExport implements FromCollection,WithHeadings
{
     /**
    * @return \Illuminate\Support\Collection
    */

    protected $uid,$app_url;

    public function __construct(String $uid,String $app_url)
    {
        $this->uid = $uid;
        $this->app_url = $app_url;
    } 

    public function collection()
    {
        $actions = ScanAction::where('uid',$this->uid)->get();
        $filter = [];
        foreach($actions as $key => $action){
            if($action->status == 1){
                $filter[$action->scan_uid][]=$action->action;
            }
        }
       
        foreach($filter as $key => $action){
            $filter[$key]=implode(", ",$action);
        }

        $notes = ScanNote::where('uid',$this->uid)->get();
        $filterNote = [];
        foreach($notes as $key => $action){
            $filterNote[$action->scan_uid][]="Note ".$action->note_list." : ".$action->note;
        }
       
        foreach($filterNote as $key => $action){
            $filterNote[$key]=implode(", ",$action);
        }
        
        $exports = ScanAction::where('uid',$this->uid)->where('app_url',$this->app_url)->select(['scan_uid','scan_name','company','delegate','email','mobile'])->get();

        foreach($exports as $key => $export){
            $exportData[$export->scan_uid]=$export;
            if($export->scan_uid){
                
                if(isset($filter[$export->scan_uid])){
                    $exportData[$export->scan_uid]->action = $filter[$export->scan_uid];
                }else{
                    $exportData[$export->scan_uid]->action = '-';
                }

                if(isset($filterNote[$export->scan_uid])){
                    $exportData[$export->scan_uid]->note = $filterNote[$export->scan_uid];
                }else{
                    $exportData[$export->scan_uid]->note = '-';
                }
            }
        }
        $arr = array();

            foreach ($exportData as $data){
                array_push($arr,$data);
           }
        
        
        return collect($arr);
    }

    public function headings(): array
    {
        return [
            'SRN (UID)',
            'Name',
            'Company',
            'Job Title',
            'Email',
            'Mobile',
            'Action',
            'Note',
        
        // etc

        ];
    }
}
