<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings; 

class SelfExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $uid,$app_url;

    public function __construct(String $uid,String $app_url)
    {
        $this->uid = $uid;
        $this->app_url = $app_url;

    }

    public function collection()
    {
        $actions = User::where('uid',$this->uid)->where('app_url',$this->app_url)
            ->select(['name', 'uid','delegate','interest','company','mobile_no','email','address','position'])->get();

        return $actions;
    }

    public function headings(): array
    {
        return [
            'Name',
            'UID',
            'Job Title',
            'Interest',
            'Company',
            'Mobile',
            'Email',
            'Address',
            'Position'

        // etc

        ];
    }
}
