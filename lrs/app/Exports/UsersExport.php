<?php
  
namespace App\Exports;
   
use App\User; 
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings; 
  
class UsersExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $app_url;

    public function __construct(String $app_url)
    {
        $this->app_url = $app_url;
    }

    public function collection()
    {
        return User::where('app_url',$this->app_url)->select(['id','name','uid', 'delegate','interest','company','position','address','email','mobile_no'])->get();
    }

    public function headings(): array
    {
        return [
            'ID-default',
            'Name',
            'UID',
            'Job Title',
            'Interest',
            'Company',
            'Position',
            'Address',
            'Email',
            'Mobile No',
        // etc

        ];
    }
}