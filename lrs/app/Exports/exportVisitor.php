<?php

namespace App\Exports;

use App\Exhibitors;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings; 

class exportVisitor implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $app_url;

    public function __construct(String $app_url)
    {
        $this->app_url = $app_url;
    }

    public function collection()
    {
        return Exhibitors::where('app_url',$this->app_url)->select(['id','uid','company','title','first_name','last_name', 'delegate','email','type','mobile','source'])->get();
    }
 
    public function headings(): array
    {
        return [
            'ID',
            'UID',
            'Company',
            'Title',
            'First Name',
            'Last Name',
            'Job Title',
            'Email',
            'Type of Business',
            'Mobile',
            'Source'
        // etc
        ];
    }
}
