<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScanAudio extends Model
{
    //
    use SoftDeletes;
    
    protected $table = 'scan_audio';

    protected $fillable = [
        'uid', 'scan_uid', 'audio','app_url','audio_list','name'
    ];

}
