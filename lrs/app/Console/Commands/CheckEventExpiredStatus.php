<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Carbon\Carbon;

use App\App;

class CheckEventExpiredStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dateOrigin = Carbon::now()->subHours(18);
        $date = $dateOrigin->format('Y-m-d');
        
        $appData = App::get();
        foreach ($appData as $key => $value) {
            if($value->expired_status == 0){
                if($date >= $value->end_date){
                    App::where('app_url',$value->app_url)->update(['expired_status' => 1]);
                }
            }
        }
        
        echo 'run';
    }
}
