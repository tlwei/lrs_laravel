<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; // <-- This is required

class ScanActionList extends Model
{
    use SoftDeletes;
    
    protected $table = 'scan_action_list';

    protected $fillable = [
        'action','app_url','action_list'
    ];
}
