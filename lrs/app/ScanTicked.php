<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScanTicked extends Model
{
    //
    protected $table = 'scan_ticked';

    protected $fillable = [
        'uid','scan_uid','brochure', 'quotation', 'sales','report','meeting','app_url'
    ];
}
