<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class App extends Model
{
    //
    protected $table = 'app';

    protected $fillable = [
        'app_url','username','password', 'background_color', 'font_size','list_icon','main_title','title_color','btn_color','btn_text','heading_img','footer_img','mic_img','user_id','start_date','end_date'
    ];

}
