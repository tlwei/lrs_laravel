<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mailContent extends Model
{
    protected $table = 'mail_content';

    protected $fillable = [
        'app_url', 'header_pic1','header_pic2','date1','date2','location1','location2','content_pic1','content_pic2','content1','content2','company','address','email','phone','facebook','instagram','twitter','LinkedIn','moreContent','CEO'
    ];
}
 