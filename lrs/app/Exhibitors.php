<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exhibitors extends Model
{
    protected $table = 'lrs_exhibitor';

    protected $fillable = [
        'uid', 'company','title','first_name','last_name','delegate','mobile','telephone','address','source','email','city','type','app_url'
    ]; 
}
