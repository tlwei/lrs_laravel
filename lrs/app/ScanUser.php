<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScanUser extends Model
{
    //
    protected $table = 'scan_user';

    protected $fillable = [
        'uid', 'scan_uid', 'name','delegate','email','mobile','company','avatar','app_url'
    ];
}
