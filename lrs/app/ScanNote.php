<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScanNote extends Model
{
    //
    use SoftDeletes;
    
    protected $table = 'scan_note';

    protected $fillable = [
        'uid', 'scan_uid','scan_name','note','app_url','note_list','company','email'
    ];
}
