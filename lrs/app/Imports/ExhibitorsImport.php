<?php
  
namespace App\Imports;
  
use App\Exhibitors; 

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
class ExhibitorsImport implements ToCollection
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    protected $app_url;

    public function __construct(String $app_url)
    {
        $this->app_url = $app_url;
    }

    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {

                Exhibitors::updateOrCreate(
                    ['uid' => $row[1],'email' => $row[7]],
                    [
                        'uid' => $row[1], 
                        'company' => $row[2],
                        'title' => $row[3],
                        'first_name' => $row[4],
                        'last_name' => $row[5],
                        'delegate' => $row[6],
                        'email' => $row[7],
                        'type' => $row[8],
                        'mobile' => $row[9],
                        'source' => $row[10],
                        'app_url' => $this->app_url,
                   ]);
                    
                //     [
                //         'uid' => $row[1], 
                //         'company' => $row[2],
                //         'title' => $row[3],
                //         'first_name' => $row[4],
                //         'last_name' => $row[5],
                //         'delegate' => $row[6],
                //         'email' => $row[7],
                //         'type' => $row[8],
                //         'mobile' => $row[9],
                //         'source' => $row[10],
                //         'app_url' => $this->app_url,
                //    ]);
            
        }
    }
}
