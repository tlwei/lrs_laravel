<?php
  
namespace App\Imports;
  
use App\User; 

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\App;

class UsersImport implements ToCollection
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    protected $app_url;

    public function __construct(String $app_url)
    { 
        $this->app_url = $app_url;
    }

    public function generateUID()  
    {
        $user_id = App::where('app_url',$this->app_url)->get();
        $id = $user_id[0]->user_id;
        $uid = $id.rand ( 10000 , 99999 );
        $res = User::where('uid',$uid)->get();
        while ($res == '[]') {
            return $uid;
        }
    }

    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            if(User::where('email',$row[8])->get() == '[]'){
                User::updateOrCreate(
                    [
                         'name' => $row[1],
                         'uid' => $this->generateUID(), 
                         'delegate' => $row[3],
                         'interest' => $row[4],
                         'company' => $row[5],
                         'password' => '123456',
                         'position' => $row[6],
                         'address' => $row[7],
                         'email' => $row[8],
                         'mobile_no' => $row[9],
                         'app_url' => $this->app_url,
                    ]);
            }else{
                User::where('email', $row[8])->update(
                    [
                         'name' => $row[1],
                         'delegate' => $row[3],
                         'interest' => $row[4],
                         'company' => $row[5],
                         'password' => '123456',
                         'position' => $row[6],
                         'address' => $row[7],
                         'email' => $row[8],
                         'mobile_no' => $row[9],
                         'app_url' => $this->app_url,
                    ]);
            }
            
        }
    }
}

// public function model(array $row)
// { 
//     return new User([
//         'name' => $row[1],
//         'uid' => count(User::withTrashed()->get())+1, 
//         'delegate' => $row[3],
//         'interest' => $row[4],
//         'company' => $row[5],
//         'password' => '123456',
//         'position' => $row[6],
//         'user_name' => $row[7],
//         'address' => $row[8],
//         'email' => $row[9],
//         'mobile_no' => $row[10],
//         'app_url' => $this->app_url,
//     ]);